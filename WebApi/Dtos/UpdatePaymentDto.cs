﻿namespace WebApi.Dtos
{
    public class UpdatePaymentDto
    {
        public string MethodName { get; set; } = string.Empty;
        public IFormFile? MethodImage { get; set; }
        public bool IsActive { get; set; }
    }
}
