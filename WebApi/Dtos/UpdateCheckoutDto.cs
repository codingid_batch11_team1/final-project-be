﻿namespace WebApi.Dtos
{
    public class UpdateCheckoutDto
    {
        public int IdCheckout { get; set; }
        public int FkIdUser { get; set; }
        public int FkIdSchedule { get; set; }
        public int FkIdCourse { get; set; }
    }
}
