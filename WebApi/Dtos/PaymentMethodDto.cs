﻿using Microsoft.AspNetCore.Http;


namespace WebApi.Dtos
{
    public class PaymentMethodDto
    {
        public string MethodName { get; set; } = string.Empty;
        public IFormFile? MethodImage { get; set; }
        public bool IsActive { get; set; }
    }
}
