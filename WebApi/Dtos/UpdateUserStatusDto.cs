﻿namespace WebApi.Dtos
{
    public class UpdateUserStatusDto
    {
        public string Role { get; set; } = string.Empty;
        public bool is_Active { get; set; }
        public string Username { get; set; } = string.Empty;
        public string Email { get; set; } = string.Empty;

    }
}
