﻿namespace WebApi.Dtos
{
    public class AddScheduleDto
    {
        public DateTime ScheduleDate { get; set; }
        public int FkIdCourse { get; set; }
    }
}
