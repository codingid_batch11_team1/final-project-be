﻿namespace WebApi.Dtos
{
    public class UpdateIsActiveDto
    {
        public string Email { get; set; }  = string.Empty;
        public Boolean IsActive { get; set; }
    }
}
