﻿namespace WebApi.Dtos
{
    public class PayCheckoutDto
    {
        public int IdUser { get; set; }
        public List<int> idSchedules { get; set; } = new List<int>();
        public int IdPaymentMethod { get; set; }
        public List<int> idCheckouts { get; set; } = new List<int> ();
    }
}
