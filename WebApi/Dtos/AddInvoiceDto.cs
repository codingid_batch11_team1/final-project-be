﻿namespace WebApi.Dtos
{
    public class AddInvoiceDto
    {
        public DateTime PaymentDate { get; set; }
        public int FkIdUser { get; set; }
        public int FkidPaymentMethod { get; set; }
    }
}
