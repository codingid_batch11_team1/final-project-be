﻿namespace WebApi.Dtos
{
    public class AddCategoryDto
    {
        public string CategoryName { get; set; } = string.Empty;
        public string CategoryDetail { get; set; } = string.Empty;
        public IFormFile? CategoryImage { get; set; }
        public IFormFile? CategoryBanner { get; set; }
        public bool IsActive { get; set; }
    }
}
