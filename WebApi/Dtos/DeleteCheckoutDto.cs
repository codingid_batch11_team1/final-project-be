﻿namespace WebApi.Dtos
{
    public class DeleteCheckoutDto
    {
        public int IdCheckout { get; set; }
    }
}
