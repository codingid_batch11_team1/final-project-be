﻿using System.ComponentModel;

namespace WebApi.Dtos
{
    public class ResetPasswordDto
    {
        [DefaultValue("reynaldi.work00@gmail.com")]
        public string Email { get; set; } = string.Empty;
        public string NewPassword { get; set; } = string.Empty;
        public string Token { get; set; } = string.Empty;
    }
}
//byRey, if there is a problem, please tell me.