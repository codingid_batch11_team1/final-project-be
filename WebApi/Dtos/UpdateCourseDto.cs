﻿namespace WebApi.Dtos
{
    public class UpdateCourseDto
    {
        public string CourseName { get; set; } = string.Empty;
        public string CourseDetail { get; set; } = string.Empty;
        public int CoursePrice { get; set; }
        public int Fk_Id_Category { get; set; }
        public IFormFile? CourseImage { get; set; }
        public bool IsActive { get; set; }
    }
}
