﻿namespace WebApi.Dtos
{
    public class AddCheckoutDto
    {
        public int FkIdUser { get; set; }
        public int FkIdSchedule { get; set; }
        public List<int> idSchedules { get; set; } = new List<int>();
    }
}
