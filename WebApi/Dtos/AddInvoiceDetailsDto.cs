﻿namespace WebApi.Dtos
{
    public class AddInvoiceDetailsDto
    {
        public int FkIdInvoice { get; set; }
        public int FkIdSchedule { get; set; }
    }
}
