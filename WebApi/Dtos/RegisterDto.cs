﻿using System.ComponentModel;

namespace WebApi.Dto
{
    public class RegisterDto
    {
        public string Username { get; set; } = string.Empty;

        [DefaultValue("hanifabdihakimhani11@gmail.com")]
        public string Email { get; set; } = string.Empty;
        [DefaultValue("12345678")]
        public string Password { get; set; } = string.Empty;
    }
}