﻿using System.ComponentModel;

namespace WebApi.Dtos
{
    public class ForgotPasswordDto 
    {
        [DefaultValue("reynaldi.work00@gmail.com")]

        //Kenapa namanya Forgot password? Karena di halaman forgot password itu diminta memasukkan email untuk di verifikasi.
        public string Email { get; set; } = string.Empty;

    }
}
//byRey, if there is a problem, please tell me.