﻿namespace WebApi.Models
{
    public class PaymentMethod
    {
        public int MethodId { get; set; }
        public string MethodName { get; set; } = string.Empty;
        public string MethodImage{ get; set; } = string.Empty;
        public bool IsActive { get; set; }
    }
}
