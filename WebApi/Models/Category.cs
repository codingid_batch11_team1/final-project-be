﻿namespace WebApi.Models
{
    public class Category
    {
        public int IdCategory { get; set; }
        public string CategoryName { get; set; } = string.Empty;
        public string CategoryDetail { get; set; } = string.Empty;
        public string CategoryImage { get; set; } = string.Empty;
        public string CategoryBanner { get; set; } = string.Empty;
        public bool IsActive { get; set; }
    }
}
