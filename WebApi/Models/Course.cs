﻿namespace WebApi.Models
{
    public class Course
    {
        public int CourseId { get; set; }   
        public string CourseName { get; set; } = string.Empty;
        public string CourseDetail { get; set; } = string.Empty;
        public int CoursePrice { get; set; }
        public string CourseImage { get; set; } = string.Empty;
        public int Fk_Id_Category { get; set; }
        public bool IsActive { get; set; }

        public Category? Category { get; set; }


    }
}
