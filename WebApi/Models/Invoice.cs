﻿using Newtonsoft.Json;

namespace WebApi.Models
{
    public class Invoice
    {
        public int IdInvoice { get; set; }

        [JsonIgnore]
        public DateTime PaymentDate { get; set; }
        [JsonProperty("paymenteDate")]
        public string FormattedDate
        {
            get { return PaymentDate.ToString("yyyy-MM-dd"); }
        }
        public int FkIdUser { get; set; }
        public int FkidPaymentMethod { get; set; }
        public PaymentMethod? PaymentMethod { get; set; }
        public User? User { get; set; }


    }
}