﻿using Newtonsoft.Json;

namespace WebApi.Models
{
    public class Schedule
    {
        public int IdSchedule { get; set; }

        [JsonIgnore]
        public DateTime ScheduleDate { get; set; }

        [JsonProperty("scheduleDate")]
        public string FormattedDate
        {
            get { return ScheduleDate.ToString("yyyy-MM-dd"); }
        }

        public int FkIdCourse { get; set; }
        public Course? Course { get; set; }
    }
}