﻿namespace WebApi.Models
{
    public class InvoiceDetails
    {
        public int IdInvoiceDetails { get; set; }
        public int FkIdInvoice { get; set;}
        public int FkIdSchedule { get; set;}

        public Schedule? Schedule { get; set; }
        public Course? Course { get; set; }
        public Category? Category { get; set; }
        public Invoice? Invoice { get; set; }

        public User? User { get; set; }


    }
}