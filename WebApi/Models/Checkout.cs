﻿namespace WebApi.Models
{
    public class Checkout
    {
        public int IdCheckout { get; set; }
        public int FkIdUser { get; set; }
        public int FkIdSchedule { get; set; }
        public User? User { get; set; }
        public Schedule? Schedule { get; set; }
        public Course? Course { get; set; }
        public Category? Category { get; set; }

        public int TotalCoursePrice { get; set; }
    }
}
