﻿using MySql.Data.MySqlClient;
using System;
using WebApi.Models;

namespace WebApi.Repositories
{
    public class CategoryRepository
    {
        private readonly string _connectionString = string.Empty;
        public CategoryRepository(IConfiguration configuration)
        {
            _connectionString = configuration.GetConnectionString("Default");
        }
        public List<Category> GetAll()
        {
            List<Category> categories = new List<Category>();
            using (MySqlConnection conn = new MySqlConnection(_connectionString))
            {
                try
                {
                    conn.Open();
                    string sql = "SELECT * FROM tb_course_category WHERE is_active = 1";
                    using (MySqlCommand cmd = new MySqlCommand(sql, conn))
                    using (MySqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            categories.Add(new Category()
                            {
                                IdCategory = reader.GetInt32("id_category"),
                                CategoryName = reader.GetString("category_name"),
                                CategoryDetail = reader.GetString("category_detail"),
                                CategoryImage = reader.GetString("category_image"),
                                CategoryBanner = reader.GetString("category_banner"),
                                IsActive = reader.GetBoolean("is_active"),
                            });
                        }
                    }
                }
                catch (MySqlException sqlEx)
                {
                    // Log SQL-specific exceptions
                    Console.WriteLine("SQL Error: " + sqlEx.ToString());
                    // Optionally, re-throw or handle differently
                    throw;
                }
                catch (Exception ex)
                {
                    // Log general exceptions
                    Console.WriteLine("An error occurred: " + ex.ToString());
                    // Optionally, re-throw the exception
                    // throw;
                }
                finally
                {
                    // Ensure the connection is closed
                    conn.Close();
                }
            }
            return categories;
        }


        public List<Category> GetAllAdmin()
        {
            List<Category> categories = new List<Category>();
            using (MySqlConnection conn = new MySqlConnection(_connectionString))
            {
                try
                {
                    conn.Open();

                    string sql = "SELECT * FROM tb_course_category";
                    using (MySqlCommand cmd = new MySqlCommand(sql, conn))
                    using (MySqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            categories.Add(new Category()
                            {
                                IdCategory = reader.GetInt32("id_category"),
                                CategoryName = reader.GetString("category_name"),
                                CategoryDetail = reader.GetString("category_detail"),
                                CategoryImage = reader.GetString("category_image"),
                                CategoryBanner = reader.GetString("category_banner"),
                                IsActive = reader.GetBoolean("is_active"),
                            });
                        }
                    }
                }
                catch (MySqlException sqlEx)
                {
                    // Log SQL-specific exceptions
                    Console.WriteLine("SQL Error: " + sqlEx.ToString());
                    // Optionally, re-throw or handle differently
                    throw;
                }
                catch (Exception ex)
                {
                    // Log general exceptions
                    Console.WriteLine("An error occurred: " + ex.ToString());
                    // Optionally, re-throw the exception
                    // throw;
                }
                finally
                {
                    // Ensure the connection is closed
                    conn.Close();
                }
            }
            return categories;
        }

        public Category? GetCategoryById(int id)
        {
            Category? category = null;

            MySqlConnection conn = new MySqlConnection(_connectionString);
            try
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("SELECT * FROM tb_course_category WHERE id_category=@IdCategory", conn);

                cmd.Parameters.AddWithValue("@IdCategory", id);

                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    category = new Category
                    {
                        IdCategory = reader.GetInt32("id_category"),
                        CategoryName = reader.GetString("category_name"),
                        CategoryDetail = reader.GetString("category_detail"),
                        CategoryImage = reader.GetString("category_image"),
                        CategoryBanner = reader.GetString("category_banner"),
                        IsActive = reader.GetBoolean("is_active"),
                    };
                }
            }
            catch (MySqlException sqlEx)
            {
                // Log SQL-specific exceptions
                Console.WriteLine("SQL Error: " + sqlEx.ToString());
                // Optionally, re-throw or handle differently
                throw;
            }
            catch (Exception ex)
            {
                // Log general exceptions
                Console.WriteLine("An error occurred: " + ex.ToString());
                // Optionally, re-throw the exception
                // throw;
            }
            finally
            {
                // Ensure the connection is closed
                conn.Close();
            }

            return category;
        }

        public Category Create(Category data)
        {
            MySqlConnection conn = new MySqlConnection(_connectionString);
            try
            {
                conn.Open();

                string sql = "INSERT INTO tb_course_category (category_name, category_detail, category_image, category_banner, is_active) VALUES (@CategoryName, @CategoryDetail, @CategoryImage, @CategoryBanner, @IsActive)";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@CategoryName", data.CategoryName);
                cmd.Parameters.AddWithValue("@CategoryDetail", data.CategoryDetail);
                cmd.Parameters.AddWithValue("@CategoryImage", data.CategoryImage);
                cmd.Parameters.AddWithValue("@CategoryBanner", data.CategoryBanner);
                cmd.Parameters.AddWithValue("@IsActive", data.IsActive);
                cmd.ExecuteNonQuery();

                data.IdCategory = (int)cmd.LastInsertedId;
            }
            catch (MySqlException sqlEx)
            {
                // Log SQL-specific exceptions
                Console.WriteLine("SQL Error: " + sqlEx.ToString());
                // Optionally, re-throw or handle differently
                throw;
            }
            catch (Exception ex)
            {
                // Log general exceptions
                Console.WriteLine("An error occurred: " + ex.ToString());
                // Optionally, re-throw the exception
                // throw;
            }
            finally
            {
                // Ensure the connection is closed
                conn.Close();
            }
            return data;
        }

        public Category Update(Category data)
        {
            MySqlConnection conn = new MySqlConnection(_connectionString);
            try
            {
                conn.Open();

                string sql = "UPDATE tb_course_category SET category_name=@CategoryName, category_detail = @CategoryDetail, category_image = @CategoryImage, category_banner = @CategoryBanner, is_active = @IsActive WHERE id_category=@IdCategory";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@CategoryName", data.CategoryName);
                cmd.Parameters.AddWithValue("@CategoryDetail", data.CategoryDetail);
                cmd.Parameters.AddWithValue("@CategoryImage", data.CategoryImage);
                cmd.Parameters.AddWithValue("@CategoryBanner", data.CategoryBanner);
                cmd.Parameters.AddWithValue("@IsActive", data.IsActive);
                cmd.Parameters.AddWithValue("@IdCategory", data.IdCategory);
                int rowsAffected = cmd.ExecuteNonQuery();

                if (rowsAffected < 1)
                {
                    //fail
                    throw new Exception("Failed to Update");
                }
            }
            catch (MySqlException sqlEx)
            {
                // Log SQL-specific exceptions
                Console.WriteLine("SQL Error: " + sqlEx.ToString());
                // Optionally, re-throw or handle differently
                throw;
            }
            catch (Exception ex)
            {
                // Log general exceptions
                Console.WriteLine("An error occurred: " + ex.ToString());
                // Optionally, re-throw the exception
                // throw;
            }
            finally
            {
                // Ensure the connection is closed
                conn.Close();
            }
            return data;
        }

        public bool Delete(int id)
        {
            MySqlConnection conn = new MySqlConnection(_connectionString);
            try
            {
                conn.Open();

                string sql = "DELETE FROM tb_course_category WHERE id_category=@IdCategory";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@IdCategory", id);
                return cmd.ExecuteNonQuery() > 0;

            }
            catch (MySqlException sqlEx)
            {
                // Log SQL-specific exceptions
                Console.WriteLine("SQL Error: " + sqlEx.ToString());
                // Optionally, re-throw or handle differently
                throw;
            }
            catch (Exception ex)
            {
                // Log general exceptions
                Console.WriteLine("An error occurred: " + ex.ToString());
                // Optionally, re-throw the exception
                // throw;
            }
            finally
            {
                // Ensure the connection is closed
                conn.Close();
            }
            return true;
        }
    }
}
