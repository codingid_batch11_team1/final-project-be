﻿using MailKit.Search;
using MySql.Data.MySqlClient;
using WebApi.Models;
using static System.Net.Mime.MediaTypeNames;

namespace WebApi.Repositories
{
    public class CheckoutRepository
    {
        private readonly string _connectionString = string.Empty;
        public CheckoutRepository(IConfiguration configuration)
        {
            _connectionString = configuration.GetConnectionString("Default");
        }
        public List<Checkout> GetCheckoutByIdUser(int idUser)
        {
            List<Checkout> checkouts = new List<Checkout>();

            using (MySqlConnection conn = new MySqlConnection(_connectionString))
            {
                try
                {
                    conn.Open();

                    string sql =
                    "SELECT " +
                        "ch.id_checkout, " +
                        "ch.fk_id_user, " +
                        "ch.fk_id_schedule, " +
                        "u.id_user, " +
                        "u.username, " +
                        "u.email, " +
                        "s.id_schedule, " +
                        "s.schedule, " +
                        "s.fk_id_course, " +
                        "c.id_course, " +
                        "c.course_name, " +
                        "c.course_detail, " +
                        "c.course_price, " +
                        "c.course_image, " +
                        "c.fk_id_category, " +
                        "cat.id_category, " + 
                        "cat.category_name, " + 
                        "cat.category_detail, " + 
                        "cat.category_image, " + 
                        "cat.category_banner, " + 
                        "cat.is_active, " +
                        "(SELECT SUM(course_price) " +
                            "FROM tb_checkout AS inner_ch " +
                            "JOIN tb_schedule AS inner_s ON inner_ch.fk_id_schedule = inner_s.id_schedule " +
                            "JOIN tb_course AS inner_c ON inner_s.fk_id_course = inner_c.id_course " +
                            "WHERE inner_ch.fk_id_user=ch.fk_id_user) AS TotalCoursePrice " +
                    "FROM " +
                        "tb_checkout AS ch " +
                    "JOIN " +
                        "tb_user AS u ON ch.fk_id_user = u.id_user " +
                    "JOIN " +
                        "tb_schedule AS s ON ch.fk_id_schedule = s.id_schedule " +
                    "JOIN " +
                        "tb_course AS c ON s.fk_id_course = c.id_course " +
                    "JOIN " + 
                        "tb_course_category AS cat ON c.fk_id_category = cat.id_category " +
                    "WHERE " +
                        "ch.fk_id_user=@IdUser AND " + 
                        "c.is_active=1 AND " +
                        "cat.is_active=1"; 


                    MySqlCommand cmd = new MySqlCommand(sql, conn);

                    cmd.Parameters.AddWithValue("IdUser", idUser);

                    using (MySqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            // Ensure that your Checkout model can accommodate all the data being read here
                            checkouts.Add(new Checkout()
                            {
                                IdCheckout = reader.GetInt32("id_checkout"),
                                FkIdUser = reader.GetInt32("fk_id_user"),
                                FkIdSchedule = reader.GetInt32("fk_id_schedule"),
                                User = new User() // Assuming you have a User class that corresponds to the structure
                                {
                                    IdUser = reader.GetInt32("id_user"),
                                    Username = reader.GetString("username"),
                                    Email = reader.GetString("email"),
                                    // ... other properties
                                },
                                Schedule = new Schedule() // Assuming you have a Schedule class
                                {
                                    // Map Schedule properties
                                    IdSchedule = reader.GetInt32("id_schedule"),
                                    ScheduleDate = DateTime.Parse(reader.GetString("schedule")),
                                    FkIdCourse = reader.GetInt32("fk_id_course"),

                                },
                                Course = new Course() // Assuming you have a Course class
                                {
                                    CourseId = reader.GetInt32("id_course"),
                                    CourseName = reader.GetString("course_name"),
                                    CourseDetail = reader.GetString("course_detail"),
                                    Fk_Id_Category = reader.GetInt32("fk_id_category"),
                                    CoursePrice = reader.GetInt32("course_price"),
                                    CourseImage = reader.GetString("course_image"),
                                },
                                Category = new Category()
                                {
                                    IdCategory = reader.GetInt32("id_category"),
                                    CategoryName = reader.GetString("category_name"),
                                    CategoryDetail = reader.GetString("category_detail"),
                                    CategoryImage = reader.GetString("category_image"),
                                    CategoryBanner = reader.GetString("category_banner"),
                                    IsActive = reader.GetBoolean("is_active"),
                                },
                                TotalCoursePrice = reader.GetInt32("TotalCoursePrice")
                            });

                        }
                    }
                }
                catch (MySqlException sqlEx)
                {
                    // Log SQL-specific exceptions
                    Console.WriteLine("SQL Error: " + sqlEx.ToString());
                    // Optionally, re-throw or handle differently
                    throw;
                }
                catch (Exception ex)
                {
                    // Log general exceptions
                    Console.WriteLine("An error occurred: " + ex.ToString());
                    // Optionally, re-throw the exception
                    // throw;
                }
                finally
                {
                    // Ensure the connection is closed
                    conn.Close();
                }
            }

            return checkouts;
        }

        public bool CheckoutExists(int fkIdUser, int scheduleId)
        {
            bool existsFlag = false; // Declare a variable to hold the result

            using (MySqlConnection conn = new MySqlConnection(_connectionString))
            {
                try
                {
                    conn.Open();

                    string sql = "SELECT COUNT(1) FROM tb_checkout WHERE fk_id_user = @FkIdUser AND fk_id_schedule = @ScheduleId";

                    MySqlCommand cmd = new MySqlCommand(sql, conn);
                    cmd.Parameters.AddWithValue("@FkIdUser", fkIdUser);
                    cmd.Parameters.AddWithValue("@ScheduleId", scheduleId); // Prevent SQL Injection

                    int exists = Convert.ToInt32(cmd.ExecuteScalar());

                    existsFlag = exists > 0; // Assign the result to the variable
                }
                catch (MySqlException sqlEx)
                {
                    // Log SQL-specific exceptions
                    Console.WriteLine("SQL Error: " + sqlEx.ToString());
                    // Optionally, re-throw or handle differently
                    throw;
                }
                catch (Exception ex)
                {
                    // Log general exceptions
                    Console.WriteLine("An error occurred: " + ex.ToString());
                    // Optionally, re-throw the exception
                    // throw;
                }
                finally
                {
                    // Ensure the connection is closed
                    conn.Close();
                }
            }

            return existsFlag; // Return the result
        }


        public Checkout Create(Checkout item)
        {
            MySqlConnection conn = new MySqlConnection(_connectionString);
            try
            {
                conn.Open();

                string sql = "INSERT INTO tb_checkout (fk_id_user, fk_id_schedule) VALUES (@FkIdUser, @FkIdSchedule)";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@FkIdUser", item.FkIdUser);
                cmd.Parameters.AddWithValue("@FkIdSchedule", item.FkIdSchedule);
                cmd.ExecuteNonQuery();

                item.IdCheckout = (int)cmd.LastInsertedId;
            }
            catch (MySqlException sqlEx)
            {
                // Log SQL-specific exceptions
                Console.WriteLine("SQL Error: " + sqlEx.ToString());
                // Optionally, re-throw or handle differently
                throw;
            }
            catch (Exception ex)
            {
                // Log general exceptions
                Console.WriteLine("An error occurred: " + ex.ToString());
                // Optionally, re-throw the exception
                // throw;
            }
            finally
            {
                // Ensure the connection is closed
                conn.Close();
            }
            return item;
        }

        public Checkout Update(Checkout item)
        {
            MySqlConnection conn = new MySqlConnection(_connectionString);
            try
            {
                conn.Open();

                string sql = "UPDATE tb_checkout SET fk_id_user=@FkIdUser, fk_id_schedule = @FkIdSchedule WHERE id_checkout=@IdCheckout";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@FkIdUser", item.FkIdUser);
                cmd.Parameters.AddWithValue("@FkIdSchedule", item.FkIdSchedule);
                cmd.Parameters.AddWithValue("@IdCheckout", item.IdCheckout);
                int rowsAffected = cmd.ExecuteNonQuery();

                if (rowsAffected < 1)
                {
                    //fail
                    throw new Exception("Failed to Update");
                }
            }
            catch (MySqlException sqlEx)
            {
                // Log SQL-specific exceptions
                Console.WriteLine("SQL Error: " + sqlEx.ToString());
                // Optionally, re-throw or handle differently
                throw;
            }
            catch (Exception ex)
            {
                // Log general exceptions
                Console.WriteLine("An error occurred: " + ex.ToString());
                // Optionally, re-throw the exception
                // throw;
            }
            finally
            {
                // Ensure the connection is closed
                conn.Close();
            }
            return item;
        }

        public bool Delete(int id)
        {
            MySqlConnection conn = new MySqlConnection(_connectionString);
            try
            {
                conn.Open();

                string sql = "DELETE FROM tb_checkout WHERE id_checkout=@IdCheckout";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@IdCheckout", id);
                return cmd.ExecuteNonQuery() > 0;

            }
            catch (MySqlException sqlEx)
            {
                // Log SQL-specific exceptions
                Console.WriteLine("SQL Error: " + sqlEx.ToString());
                // Optionally, re-throw or handle differently
                throw;
            }
            catch (Exception ex)
            {
                // Log general exceptions
                Console.WriteLine("An error occurred: " + ex.ToString());
                // Optionally, re-throw the exception
                // throw;
            }
            finally
            {
                // Ensure the connection is closed
                conn.Close();
            }
            return true;
        }

        public void CreateInvoiceAndInvoiceDetails(int idUser, List<int> idSchedules, int idPaymentMethod, List<int> idCheckouts)
        {
            MySqlConnection conn = new MySqlConnection(_connectionString);
            conn.Open();
            MySqlTransaction transaction = conn.BeginTransaction();
            try
            {
                int idInvoice;
                DateTime PaymentDate = DateTime.Now;

                using (var cmd = new MySqlCommand("INSERT INTO tb_invoice(payment_date, fk_id_user, fk_id_payment_method) VALUES (@PaymentDate, @IdUser, @IdPaymentMethod)", conn))
                {
                    cmd.Parameters.AddWithValue("@IdUser", idUser);
                    cmd.Parameters.AddWithValue("@IdPaymentMethod", idPaymentMethod);
                    cmd.Parameters.AddWithValue("@PaymentDate", PaymentDate);
                    cmd.Transaction = transaction;
                    cmd.ExecuteNonQuery();

                    idInvoice = (int)cmd.LastInsertedId;
                }

                foreach (int idSchedule in idSchedules)
                {
                    using (var cmd = new MySqlCommand("INSERT INTO tb_invoice_details(fk_id_invoice, fk_id_schedule) VALUES (@IdInvoice, @IdSchedule)", conn))
                    {
                        cmd.Parameters.AddWithValue("@IdInvoice", idInvoice);
                        cmd.Parameters.AddWithValue("@IdSchedule", idSchedule);
                        cmd.Transaction = transaction;
                        cmd.ExecuteNonQuery();
                    }
                }

                // Check if idCheckouts is empty
                if (idCheckouts.Count == 0)
                {
                    // Delete from tb_checkout based on idUser and each idSchedule
                    foreach (int idSchedule in idSchedules)
                    {
                        using (var cmd = new MySqlCommand("DELETE FROM tb_checkout WHERE fk_id_user=@IdUser AND fk_id_schedule=@IdSchedule", conn))
                        {
                            cmd.Parameters.AddWithValue("@IdUser", idUser);
                            cmd.Parameters.AddWithValue("@IdSchedule", idSchedule);
                            cmd.Transaction = transaction;
                            cmd.ExecuteNonQuery();
                        }
                    }
                }
                else
                {
                    foreach (int idCheckout in idCheckouts)
                    {
                        using (var cmd = new MySqlCommand("DELETE FROM tb_checkout WHERE id_checkout=@IdCheckout", conn))
                        {
                            cmd.Parameters.AddWithValue("@IdCheckout", idCheckout);
                            cmd.Transaction = transaction;
                            cmd.ExecuteNonQuery();
                        }
                    }
                    // Existing code to handle non-empty idCheckouts...
                }


                transaction.Commit();
            }

            catch (MySqlException sqlEx)
            {
                // Log SQL-specific exceptions
                Console.WriteLine("SQL Error: " + sqlEx.ToString());
                // Optionally, re-throw or handle differently
                throw;
            }
            catch (Exception ex)
            {
                // Log general exceptions
                Console.WriteLine("An error occurred: " + ex.ToString());
                // Optionally, re-throw the exception
                // throw;
            }
            finally
            {
                // Ensure the connection is closed
                conn.Close();
            }
        }
    }
}
