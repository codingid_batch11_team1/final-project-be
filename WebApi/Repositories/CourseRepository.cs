﻿using MySql.Data.MySqlClient;
using WebApi.Models;

namespace WebApi.Repositories
{
    public class CourseRepository
    {
        private readonly string _connectionString = string.Empty;
        public CourseRepository(IConfiguration configuration)
        {
            _connectionString = configuration.GetConnectionString("Default");
        }
        public List<Course> GetAll(int? limit, int? categoryId, int? courseId)
        {
            List<Course> courses = new List<Course>();

            MySqlConnection conn = new MySqlConnection(_connectionString);
            try
            {
                Console.WriteLine("Connecting to MySQL...");
                // Perform database operations

                {
                    conn.Open();

                    string sql = "";
                    if (limit == null && categoryId == null && courseId == null )
                    {
                        sql = "SELECT " +
                               "c.id_course, " +
                               "c.course_name, " +
                               "c.course_detail, " +
                               "c.course_price, " +
                               "c.course_image, " +
                               "c.fk_id_category, " +
                               "c.is_active, " +
                               "cc.id_category, " +
                               "cc.category_name " +
                           "FROM " +
                               "tb_course AS c " +
                           "JOIN " +
                               "tb_course_category AS cc ON c.fk_id_category = cc.id_category " +
                           "WHERE " +
                               "c.is_active = 1";
                    }
                    else if (limit != null && categoryId == null && courseId == null)
                    {
                        sql = "SELECT " +
                               "c.id_course, " +
                               "c.course_name, " +
                               "c.course_detail, " +
                               "c.course_price, " +
                               "c.course_image, " +
                               "c.fk_id_category, " +
                               "c.is_active, " +
                               "cc.id_category, " +
                               "cc.category_name " +
                           "FROM " +
                               "tb_course AS c " +
                           "JOIN " +
                               "tb_course_category AS cc ON c.fk_id_category = cc.id_category " +
                           "WHERE " +
                               "c.is_active = 1 AND cc.is_active=1 " +
                           "LIMIT @Limit ";
                    }
                    else if (limit != null && categoryId != null && courseId == null)
                    {
                        sql = "SELECT " +
                               "c.id_course, " +
                               "c.course_name, " +
                               "c.course_detail, " +
                               "c.course_price, " +
                               "c.course_image, " +
                               "c.is_active, " +
                               "c.fk_id_category, " +
                               "cc.id_category, " +
                               "cc.category_name " +
                           "FROM " +
                               "tb_course AS c " +
                           "JOIN " +
                               "tb_course_category AS cc ON c.fk_id_category = cc.id_category " +
                           "WHERE " +
                           "cc.id_category=@CategoryId AND c.is_active = 1  " +
                           "LIMIT @Limit";
                    }
                    else
                    {
                        sql = "SELECT " +
                               "c.id_course, " +
                               "c.course_name, " +
                               "c.course_detail, " +
                               "c.course_price, " +
                               "c.course_image, " +
                               "c.fk_id_category, " +
                               "c.is_active, " +
                               "cc.id_category, " +
                               "cc.category_name " +
                           "FROM " +
                               "tb_course AS c " +
                           "JOIN " +
                               "tb_course_category AS cc ON c.fk_id_category = cc.id_category " +
                           "WHERE " +
                           "cc.id_category=@CategoryId AND c.id_course!=@courseId AND c.is_active = 1 " +
                           "LIMIT @Limit";
                    }


                    MySqlCommand cmd = new MySqlCommand(sql, conn);

                    cmd.Parameters.AddWithValue("@Limit", limit);
                    cmd.Parameters.AddWithValue("@CategoryId", categoryId);
                    cmd.Parameters.AddWithValue("@courseId", courseId);

                    MySqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        courses.Add(new Course()
                        {
                            CourseId = reader.GetInt32("id_course"),
                            CourseName = reader.GetString("course_name"),
                            CourseDetail = reader.GetString("course_detail"),
                            Fk_Id_Category = reader.GetInt32("fk_id_category"),
                            CoursePrice = reader.GetInt32("course_price"),
                            CourseImage = reader.GetString("course_image"),
                            IsActive = reader.GetBoolean("is_active"),

                            Category = new Category()
                            {
                                IdCategory = reader.GetInt32("id_category"),
                                CategoryName = reader.GetString("category_name"),
                            },//category class

                        });
                   }   
                }
            }

            catch (MySqlException sqlEx)
            {
                // Log SQL-specific exceptions
                Console.WriteLine("SQL Error: " + sqlEx.ToString());
                // Optionally, re-throw or handle differently
                throw;
            }
            catch (Exception ex)
            {
                // Log general exceptions
                Console.WriteLine("An error occurred: " + ex.ToString());
                // Optionally, re-throw the exception
                // throw;
            }
            finally
            {
                // Ensure the connection is closed
                conn.Close();
            }
            Console.WriteLine("Done.");

            return courses;
        }

        public List<Course> GetAllAdmin()
        {
            List<Course> courses = new List<Course>();

            MySqlConnection conn = new MySqlConnection(_connectionString);
            try
            {
                Console.WriteLine("Connecting to MySQL...");
                // Perform database operations

                {
                    conn.Open();

                    string sql = "SELECT " +
                              "c.id_course, " +
                              "c.course_name, " +
                              "c.course_detail, " +
                              "c.course_price, " +
                              "c.course_image, " +
                              "c.fk_id_category, " +
                              "c.is_active, " +
                              "cc.id_category, " +
                              "cc.category_name " +
                          "FROM " +
                              "tb_course AS c " +
                          "JOIN " +
                              "tb_course_category AS cc ON c.fk_id_category = cc.id_category ";


                    MySqlCommand cmd = new MySqlCommand(sql, conn);

                    MySqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        courses.Add(new Course()
                        {
                            CourseId = reader.GetInt32("id_course"),
                            CourseName = reader.GetString("course_name"),
                            CourseDetail = reader.GetString("course_detail"),
                            Fk_Id_Category = reader.GetInt32("fk_id_category"),
                            CoursePrice = reader.GetInt32("course_price"),
                            CourseImage = reader.GetString("course_image"),
                            IsActive = reader.GetBoolean("is_active"),

                            Category = new Category()
                            {
                                IdCategory = reader.GetInt32("id_category"),
                                CategoryName = reader.GetString("category_name"),
                            },//category class

                        });
                    }
                }
            }

            catch (MySqlException sqlEx)
            {
                // Log SQL-specific exceptions
                Console.WriteLine("SQL Error: " + sqlEx.ToString());
                // Optionally, re-throw or handle differently
                throw;
            }
            catch (Exception ex)
            {
                // Log general exceptions
                Console.WriteLine("An error occurred: " + ex.ToString());
                // Optionally, re-throw the exception
                // throw;
            }
            finally
            {
                // Ensure the connection is closed
                conn.Close();
            }
            Console.WriteLine("Done.");

            return courses;
        }

        public Course? GetCourseById(int id)
        {
            Course? course = null;

            MySqlConnection conn = new MySqlConnection(_connectionString);
            try
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand(
                               "SELECT " +
                               "c.id_course, " +
                               "c.course_name, " +
                               "c.course_detail, " +
                               "c.course_price, " +
                               "c.course_image, " +
                               "c.fk_id_category, " +
                               "c.is_active, " +
                               "cc.category_name " +
                           "FROM " +
                               "tb_course AS c " +
                           "JOIN " +
                               "tb_course_category AS cc ON c.fk_id_category = cc.id_category", conn);

                cmd.Parameters.AddWithValue("@IdCourse", id);

                MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {

                    course = new Course
                    {
                        CourseId = reader.GetInt32("id_course"),
                        CourseName = reader.GetString("course_name"),
                        CourseDetail = reader.GetString("course_detail"),
                        Fk_Id_Category = reader.GetInt32("fk_id_category"),
                        CoursePrice = reader.GetInt32("course_price"),
                        CourseImage = reader.GetString("course_image"),
                        IsActive = reader.GetBoolean("is_active"),

                        Category = new Category()
                        {
                            CategoryName = reader.GetString("category_name"),
                        }//category class
                    };
                }
            }
            catch (MySqlException sqlEx)
            {
                // Log SQL-specific exceptions
                Console.WriteLine("SQL Error: " + sqlEx.ToString());
                // Optionally, re-throw or handle differently
                throw;
            }
            catch (Exception ex)
            {
                // Log general exceptions
                Console.WriteLine("An error occurred: " + ex.ToString());
                // Optionally, re-throw the exception
                // throw;
            }
            finally
            {
                // Ensure the connection is closed
                conn.Close();
            }

            return course;
        }
        public Course Create(Course item)
        {
            MySqlConnection conn = new MySqlConnection(_connectionString);
            try
            {
                conn.Open();

                string sql = "INSERT INTO tb_course (course_name, course_detail, fk_id_category, course_price, course_image, is_active) VALUES (@CourseName, @CourseDetail, @Fk_Id_Category,@CoursePrice, @CourseImage, @IsActive)";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@CourseName", item.CourseName);
                cmd.Parameters.AddWithValue("@CourseDetail", item.CourseDetail);
                cmd.Parameters.AddWithValue("@Fk_Id_Category", item.Fk_Id_Category);
                cmd.Parameters.AddWithValue("@CoursePrice", item.CoursePrice);
                cmd.Parameters.AddWithValue("@CourseImage", item.CourseImage);
                cmd.Parameters.AddWithValue("@IsActive", item.IsActive);
                cmd.ExecuteNonQuery();

                item.CourseId = (int)cmd.LastInsertedId;
            }
            catch (MySqlException sqlEx)
            {
                // Log SQL-specific exceptions
                Console.WriteLine("SQL Error: " + sqlEx.ToString());
                // Optionally, re-throw or handle differently
                throw;
            }
            catch (Exception ex)
            {
                // Log general exceptions
                Console.WriteLine("An error occurred: " + ex.ToString());
                // Optionally, re-throw the exception
                // throw;
            }
            finally
            {
                // Ensure the connection is closed
                conn.Close();
            }

            return item;
        }
        public Course Update(Course item)
        {
            MySqlConnection conn = new MySqlConnection(_connectionString);
            try
            {
                conn.Open();

                string sql = "UPDATE tb_course SET course_name=@CourseName, course_detail=@CourseDetail, fk_id_category=@Fk_Id_Category, course_price=@CoursePrice, course_image=@CourseImage, is_active=@IsActive WHERE id_course=@CourseId";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@CourseName", item.CourseName);
                cmd.Parameters.AddWithValue("@CourseDetail", item.CourseDetail);
                cmd.Parameters.AddWithValue("@Fk_Id_Category", item.Fk_Id_Category);
                cmd.Parameters.AddWithValue("@CourseImage", item.CourseImage);
                cmd.Parameters.AddWithValue("@CoursePrice", item.CoursePrice);
                cmd.Parameters.AddWithValue("@CourseId", item.CourseId);
                cmd.Parameters.AddWithValue("@IsActive", item.IsActive);
                int rowsAffected = cmd.ExecuteNonQuery();

                if (rowsAffected < 1)
                {
                    //fail
                    throw new Exception("Failed to Update");
                }
            }
            catch (MySqlException sqlEx)
            {
                // Log SQL-specific exceptions
                Console.WriteLine("SQL Error: " + sqlEx.ToString());
                // Optionally, re-throw or handle differently
                throw;
            }
            catch (Exception ex)
            {
                // Log general exceptions
                Console.WriteLine("An error occurred: " + ex.ToString());
                // Optionally, re-throw the exception
                // throw;
            }
            finally
            {
                // Ensure the connection is closed
                conn.Close();
            }

            return item;
        }

        public bool Delete(int id)
        {
            MySqlConnection conn = new MySqlConnection(_connectionString);
            try
            {
                conn.Open();

                string sql = "DELETE FROM tb_course WHERE course_id=@CourseId";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@CourseId", id);
                return cmd.ExecuteNonQuery() > 0;

            }
            catch (MySqlException sqlEx)
            {
                // Log SQL-specific exceptions
                Console.WriteLine("SQL Error: " + sqlEx.ToString());
                // Optionally, re-throw or handle differently
                throw;
            }
            catch (Exception ex)
            {
                // Log general exceptions
                Console.WriteLine("An error occurred: " + ex.ToString());
                // Optionally, re-throw the exception
                // throw;
            }
            finally
            {
                // Ensure the connection is closed
                conn.Close();
            }
            return true;
        }
    }
}
