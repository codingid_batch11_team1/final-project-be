﻿using MySql.Data.MySqlClient;
using WebApi.Models;

namespace WebApi.Repositories
{
    public class InvoiceRepository
    {
        private readonly string _connectionString = string.Empty;
        public InvoiceRepository(IConfiguration configuration)
        {
            _connectionString = configuration.GetConnectionString("Default");
        }
        public List<Invoice> GetAll(int idUser)
        {
            List<Invoice> invoices = new List<Invoice>();

            using (MySqlConnection conn = new MySqlConnection(_connectionString))
            {
                try
                {
                    Console.WriteLine("Connecting to MySQL...");
                    conn.Open();

                    string sql =
                        "SELECT " +
                        "inv.id_invoice, " +
                        "inv.payment_date, " +
                        "inv.fk_id_user, " +
                        "inv.fk_id_payment_method, " +
                        "u.id_user, " +
                        "u.username, " +
                        "pm.method_name, " +
                        "pm.method_image " +
                        "FROM " +
                        "tb_invoice AS inv " +
                        "JOIN " +
                        "tb_user AS u ON inv.fk_id_user = u.id_user " +
                        "JOIN " +
                        "tb_payment_method AS pm ON inv.fk_id_payment_method = pm.id_payment_method " +
                        "WHERE " +
                        "inv.fk_id_user = @Id_User";

                    using (MySqlCommand cmd = new MySqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@Id_User", idUser);

                        using (MySqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                invoices.Add(new Invoice()
                                {
                                    IdInvoice = reader.GetInt32("id_invoice"),
                                    PaymentDate = reader.GetDateTime("payment_date"),
                                    FkIdUser = reader.GetInt32("fk_id_user"),
                                    FkidPaymentMethod = reader.GetInt32("fk_id_payment_method"),

                                    PaymentMethod = new PaymentMethod()
                                    {
                                        MethodName = reader.GetString("method_name"),
                                        MethodImage = reader.GetString("method_image"),
                                    }, // PaymentMethod class

                                    User = new User()
                                    {
                                        IdUser = reader.GetInt32("id_user"),
                                        Username = reader.GetString("username"),
                                    }, // User class
                                });
                            }
                        }
                    }
                }
                catch (MySqlException sqlEx)
                {
                    // Log SQL-specific exceptions
                    Console.WriteLine("SQL Error: " + sqlEx.ToString());
                    // Optionally, re-throw or handle differently
                    throw;
                }
                catch (Exception ex)
                {
                    // Log general exceptions
                    Console.WriteLine("An error occurred: " + ex.ToString());
                    // Optionally, re-throw the exception
                    // throw;
                }
                finally
                {
                    // Ensure the connection is closed
                    conn.Close();
                }
            }

            return invoices;
        }

        public List<Invoice> GetAllInvoiceAdmin()
        {
            List<Invoice> invoicesA = new List<Invoice>();

            MySqlConnection conn = new MySqlConnection(_connectionString);
            try
            {
                Console.WriteLine("Connecting to MySQL...");
                conn.Open();
                // Perform database operations

                string sql =
                "SELECT " +
                "inv.id_invoice, " +
                "inv.payment_date, " +
                "inv.fk_id_user, " +
                "inv.fk_id_payment_method, " +
                "u.id_user, " +
                "u.username, " +
                "pm.method_name, " +
                "pm.method_image " +
                "FROM " +
                "tb_invoice AS inv " +
                "JOIN " +
                "tb_user AS u ON inv.fk_id_user = u.id_user " +
                "JOIN " +
                "tb_payment_method AS pm ON inv.fk_id_payment_method = pm.id_payment_method";


                MySqlCommand cmd = new MySqlCommand(sql, conn);

                using (MySqlDataReader reader = cmd.ExecuteReader())
                    while (reader.Read())
                    {
                        invoicesA.Add(new Invoice()
                        {
                            IdInvoice = reader.GetInt32("id_invoice"),
                            PaymentDate = reader.GetDateTime("payment_date"),
                            FkIdUser = reader.GetInt32("fk_id_user"),
                            FkidPaymentMethod = reader.GetInt32("fk_id_payment_method"),

                            PaymentMethod = new PaymentMethod()
                            {
                                MethodName = reader.GetString("method_name"),
                                MethodImage = reader.GetString("method_image"),
                            }, // PaymentMethod class

                            User = new User()
                            {
                                IdUser = reader.GetInt32("id_user"),
                                Username = reader.GetString("username"),
                            }, // User class
                        });

                    }
            }
            catch (MySqlException sqlEx)
            {
                // Log SQL-specific exceptions
                Console.WriteLine("SQL Error: " + sqlEx.ToString());
                // Optionally, re-throw or handle differently
                throw;
            }
            catch (Exception ex)
            {
                // Log general exceptions
                Console.WriteLine("An error occurred: " + ex.ToString());
                // Optionally, re-throw the exception
                // throw;
            }
            finally
            {
                // Ensure the connection is closed
                conn.Close();
            }
            Console.WriteLine("Done.");

            return invoicesA;
        }
        public Invoice Create(Invoice item)
        {
            MySqlConnection conn = new MySqlConnection(_connectionString);
            try
            {
                conn.Open();

                string sql = "INSERT INTO tb_invoice (id_invoice, payment_date, fk_id_user, fk_id_payment_method) VALUES (@IdInvoice, @PaymentDate, @FkIdUser,@FkidPaymentMethod)";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@IdInvoice", item.IdInvoice);
                cmd.Parameters.AddWithValue("@PaymentDate", item.PaymentDate);
                cmd.Parameters.AddWithValue("@FkIdUser", item.FkIdUser);
                cmd.Parameters.AddWithValue("@FkidPaymentMethod", item.FkidPaymentMethod);
                cmd.ExecuteNonQuery();

                item.IdInvoice = (int)cmd.LastInsertedId;
            }
            catch (MySqlException sqlEx)
            {
                // Log SQL-specific exceptions
                Console.WriteLine("SQL Error: " + sqlEx.ToString());
                // Optionally, re-throw or handle differently
                throw;
            }
            catch (Exception ex)
            {
                // Log general exceptions
                Console.WriteLine("An error occurred: " + ex.ToString());
                // Optionally, re-throw the exception
                // throw;
            }
            finally
            {
                // Ensure the connection is closed
                conn.Close();
            }
            return item;
        }
    }
}
