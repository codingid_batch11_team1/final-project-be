﻿using MySql.Data.MySqlClient;
using Newtonsoft.Json.Linq;
using WebApi.Models;

namespace WebApi.Repositories
{
    public class UserRepository
    {
        private readonly string _connStr = string.Empty;

        //Dependency Injection
        public UserRepository(IConfiguration configuration)
        {
            //di jalankan pertama kali ketika object di buat
            _connStr = configuration.GetConnectionString("Default");
        }

        public List<User> GetAllUsers() 
        {
            List<User> users = new List<User>();
            using (MySqlConnection conn = new MySqlConnection(_connStr))
            {
                try
                {
                    conn.Open();

                    string sql = "SELECT * FROM tb_user";
                    using (MySqlCommand cmd = new MySqlCommand(sql, conn))
                    using (MySqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            users.Add(new User()
                            {
                                IdUser = reader.GetInt32("id_user"),
                                Username = reader.GetString("username"),
                                Email = reader.GetString("email"),
                                Role = reader.GetString("role"),
                                is_Active = reader.GetBoolean("is_active")
                            });
                        }
                    }
                }
                catch (MySqlException sqlEx)
                {
                    // Log SQL-specific exceptions
                    Console.WriteLine("SQL Error: " + sqlEx.ToString());
                    // Optionally, re-throw or handle differently
                    throw;
                }
                catch (Exception ex)
                {
                    // Log general exceptions
                    Console.WriteLine("An error occurred: " + ex.ToString());
                    // Optionally, re-throw the exception
                    // throw;
                }
                finally
                {
                    // Ensure the connection is closed
                    conn.Close();
                }

                return users;
            }
        }

        public User? GetByEmailAndPassword(string email, string password)
        {
            User? user = null;

            //get connection to database
            MySqlConnection conn = new MySqlConnection(_connStr);
            try
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("SELECT id_user, email, role FROM tb_user WHERE email=@Email AND password=@Password AND is_active = 1", conn);

                cmd.Parameters.AddWithValue("@Email", email);
                cmd.Parameters.AddWithValue("@Password", password);

                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    user = new User
                    {
                        IdUser = reader.GetInt32("id_user"),
                        Email = reader.GetString("email"),
                        Role = reader.GetString("role")
                    };
                }
            }
            catch (MySqlException sqlEx)
            {
                // Log SQL-specific exceptions
                Console.WriteLine("SQL Error: " + sqlEx.ToString());
                // Optionally, re-throw or handle differently
                throw;
            }
            catch (Exception ex)
            {
                // Log general exceptions
                Console.WriteLine("An error occurred: " + ex.ToString());
                // Optionally, re-throw the exception
                // throw;
            }
            finally
            {
                // Ensure the connection is closed
                conn.Close();
            }

            return user;
        }

        public User? GetByEmail(string email)
        {
            User? user = null;

            //get connection to database
            MySqlConnection conn = new MySqlConnection(_connStr);
            try
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("SELECT id_user, email, role FROM tb_user WHERE email=@Email", conn);

                cmd.Parameters.AddWithValue("@Email", email);

                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    user = new User
                    {
                        IdUser = reader.GetInt32("id_user"),
                        Email = reader.GetString("email"),
                        Role = reader.GetString("role"),
                    };
                }
            }
            catch (MySqlException sqlEx)
            {
                // Log SQL-specific exceptions
                Console.WriteLine("SQL Error: " + sqlEx.ToString());
                // Optionally, re-throw or handle differently
                throw;
            }
            catch (Exception ex)
            {
                // Log general exceptions
                Console.WriteLine("An error occurred: " + ex.ToString());
                // Optionally, re-throw the exception
                // throw;
            }
            finally
            {
                // Ensure the connection is closed
                conn.Close();
            }

            return user;
        }

        public User? GetByEmailAndResetToken(string email, string resetToken)
        {
            User? user = null;

            //get connection to database
            MySqlConnection conn = new MySqlConnection(_connStr);
            try
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("SELECT id_user, email, role FROM tb_user WHERE email=@Email AND reset_password_token=@ResetToken", conn);

                cmd.Parameters.AddWithValue("@Email", email);
                cmd.Parameters.AddWithValue("@ResetToken", resetToken);

                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    user = new User
                    {
                        IdUser = reader.GetInt32("id_user"),
                        Email = reader.GetString("email"),
                        Role = reader.GetString("role"),
                    };
                }
            }
            catch (MySqlException sqlEx)
            {
                // Log SQL-specific exceptions
                Console.WriteLine("SQL Error: " + sqlEx.ToString());
                // Optionally, re-throw or handle differently
                throw;
            }
            catch (Exception ex)
            {
                // Log general exceptions
                Console.WriteLine("An error occurred: " + ex.ToString());
                // Optionally, re-throw the exception
                // throw;
            }
            finally
            {
                // Ensure the connection is closed
                conn.Close();
            }

            return user;
        }


        public User? Create(string username, string email, string password)//Untuk Register
        {
            User? user = null;
            //get connection to database
            MySqlConnection conn = new MySqlConnection(_connStr);
            try
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("INSERT INTO tb_user(username, email, password) VALUES (@Username, @Email, @Password)", conn);

                cmd.Parameters.AddWithValue("@Username", username);
                cmd.Parameters.AddWithValue("@Email", email);
                cmd.Parameters.AddWithValue("@Password", password);

                cmd.ExecuteNonQuery();

            }
            catch (MySqlException sqlEx)
            {
                // Log SQL-specific exceptions
                Console.WriteLine("SQL Error: " + sqlEx.ToString());
                // Optionally, re-throw or handle differently
                throw;
            }
            catch (Exception ex)
            {
                // Log general exceptions
                Console.WriteLine("An error occurred: " + ex.ToString());
                // Optionally, re-throw the exception
                // throw;
            }
            finally
            {
                // Ensure the connection is closed
                conn.Close();
            }
            return user;
        }

        public bool UpdateIsActive(int userId, bool isActive)//Untuk update isActive agar user bisa login
        {
            bool isSuccess = false;

            MySqlConnection conn = new MySqlConnection(_connStr);

            try
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("UPDATE tb_user SET is_active = @isActive WHERE id_user = @Id", conn);

                cmd.Parameters.AddWithValue("@Id", userId);
                cmd.Parameters.AddWithValue("@isActive", isActive);

                cmd.ExecuteNonQuery();

                isSuccess = true;

            }
            catch (MySqlException sqlEx)
            {
                // Log SQL-specific exceptions
                Console.WriteLine("SQL Error: " + sqlEx.ToString());
                // Optionally, re-throw or handle differently
                throw;
            }
            catch (Exception ex)
            {
                // Log general exceptions
                Console.WriteLine("An error occurred: " + ex.ToString());
                // Optionally, re-throw the exception
                // throw;
            }
            finally
            {
                // Ensure the connection is closed
                conn.Close();
            }

            return isSuccess;
        }

        public User CreateUserByAdmin(User data)
        {
            MySqlConnection conn = new MySqlConnection(_connStr);
            try
            {
                conn.Open();
                string sql = "INSERT INTO tb_user(username, email, password, role, is_active) VALUES (@Username, @Email, @Password, @Role, @IsActive)";
                MySqlCommand cmd = new MySqlCommand(sql, conn);

                cmd.Parameters.AddWithValue("@Username", data.Username);
                cmd.Parameters.AddWithValue("@Email", data.Email);
                cmd.Parameters.AddWithValue("@Password", data.Password);
                cmd.Parameters.AddWithValue("@Role", data.Role);
                cmd.Parameters.AddWithValue("@IsActive", data.is_Active);

                cmd.ExecuteNonQuery();

                data.IdUser = (int)cmd.LastInsertedId;
            }
            catch (MySqlException sqlEx)
            {
                // Log SQL-specific exceptions
                Console.WriteLine("SQL Error: " + sqlEx.ToString());
                // Optionally, re-throw or handle differently
                throw;
            }
            catch (Exception ex)
            {
                // Log general exceptions
                Console.WriteLine("An error occurred: " + ex.ToString());
                // Optionally, re-throw the exception
                // throw;
            }
            finally
            {
                // Ensure the connection is closed
                conn.Close();
            }
            return data;
        }

        public bool InsertResetPasswordToken(int userId, string token)
        {
            bool isSuccess = false;

            //get connection to database
            MySqlConnection conn = new MySqlConnection(_connStr);
            try
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("UPDATE tb_user SET reset_password_token=@Token WHERE id_user=@Id", conn);

                cmd.Parameters.AddWithValue("@Id", userId);
                cmd.Parameters.AddWithValue("@Token", token);

                cmd.ExecuteNonQuery();

                isSuccess = true;
            }
            catch (MySqlException sqlEx)
            {
                // Log SQL-specific exceptions
                Console.WriteLine("SQL Error: " + sqlEx.ToString());
                // Optionally, re-throw or handle differently
                throw;
            }
            catch (Exception ex)
            {
                // Log general exceptions
                Console.WriteLine("An error occurred: " + ex.ToString());
                // Optionally, re-throw the exception
                // throw;
            }
            finally
            {
                // Ensure the connection is closed
                conn.Close();
            }

            return isSuccess;
        }

        public bool UpdatePassword(int userId, string newPassword)
        {
            bool isSuccess = false;

            //get connection to database
            MySqlConnection conn = new MySqlConnection(_connStr);
            try
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("UPDATE tb_user SET password=@NewPassword WHERE id_user=@Id", conn);

                cmd.Parameters.AddWithValue("@Id", userId);
                cmd.Parameters.AddWithValue("@NewPassword", newPassword);

                cmd.ExecuteNonQuery();

                isSuccess = true;
            }
            catch (MySqlException sqlEx)
            {
                // Log SQL-specific exceptions
                Console.WriteLine("SQL Error: " + sqlEx.ToString());
                // Optionally, re-throw or handle differently
                throw;
            }
            catch (Exception ex)
            {
                // Log general exceptions
                Console.WriteLine("An error occurred: " + ex.ToString());
                // Optionally, re-throw the exception
                // throw;
            }
            finally
            {
                // Ensure the connection is closed
                conn.Close();
            }

            return isSuccess;
        }

        public bool UpdateUserStatus(User item) 
        {
            bool isSuccess = false;

            MySqlConnection conn = new MySqlConnection(_connStr);
            try
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("UPDATE tb_user SET is_active=@IsActive, role=@Role, username=@Username, email=@Email WHERE id_user=@Id", conn);

                cmd.Parameters.AddWithValue("@Id", item.IdUser);
                cmd.Parameters.AddWithValue("@Username", item.Username);
                cmd.Parameters.AddWithValue("@Email", item.Email);
                cmd.Parameters.AddWithValue("@Role", item.Role);
                cmd.Parameters.AddWithValue("@isActive", item.is_Active);

                cmd.ExecuteNonQuery();

                isSuccess = true;
            }
            catch (MySqlException sqlEx)
            {
                // Log SQL-specific exceptions
                Console.WriteLine("SQL Error: " + sqlEx.ToString());
                // Optionally, re-throw or handle differently
                throw;
            }
            catch (Exception ex)
            {
                // Log general exceptions
                Console.WriteLine("An error occurred: " + ex.ToString());
                // Optionally, re-throw the exception
                // throw;
            }
            finally
            {
                // Ensure the connection is closed
                conn.Close();
            }

            return isSuccess;
        }
    }
}
