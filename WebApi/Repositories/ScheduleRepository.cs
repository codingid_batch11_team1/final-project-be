﻿using MailKit.Search;
using MySql.Data.MySqlClient;
using WebApi.Models;

namespace WebApi.Repositories
{
    public class ScheduleRepository
    {
        private readonly string _connectionString = string.Empty;
        public ScheduleRepository(IConfiguration configuration)
        {
            _connectionString = configuration.GetConnectionString("Default");
        }
        public List<Schedule> GetAll()
        {
            List<Schedule> schedules = new List<Schedule>();

            MySqlConnection conn = new MySqlConnection(_connectionString);
            try
            {
                conn.Open();

                string sql =
                       "SELECT " +
                           "s.id_schedule, " +
                           "s.schedule, " +
                           "s.fk_id_course," +
                           "c.id_course, " +
                           "c.course_name, " +
                           "c.course_detail, " +
                           "c.course_price, " +
                           "c.course_image, " +
                           "c.fk_id_category " +
                       "FROM " +
                           "tb_schedule AS s " +
                       "JOIN " +
                           "tb_course AS c ON s.fk_id_course = c.id_course";

                MySqlCommand cmd = new MySqlCommand(sql, conn);
                MySqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    schedules.Add(new Schedule()
                    {
                        IdSchedule = reader.GetInt32("id_schedule"),
                        ScheduleDate = DateTime.Parse(reader.GetString("schedule")),
                        FkIdCourse = reader.GetInt32("fk_id_course"),
                        Course = new Course() // Assuming you have a Course class
                        {
                            CourseId = reader.GetInt32("id_course"),
                            CourseName = reader.GetString("course_name"),
                            CourseDetail = reader.GetString("course_detail"),
                            Fk_Id_Category = reader.GetInt32("fk_id_category"),
                            CoursePrice = reader.GetInt32("course_price"),
                            CourseImage = reader.GetString("course_image"),
                            // ... other properties
                        },
                    });
                }
            }
            catch (MySqlException sqlEx)
            {
                // Log SQL-specific exceptions
                Console.WriteLine("SQL Error: " + sqlEx.ToString());
                // Optionally, re-throw or handle differently
                throw;
            }
            catch (Exception ex)
            {
                // Log general exceptions
                Console.WriteLine("An error occurred: " + ex.ToString());
                // Optionally, re-throw the exception
                // throw;
            }
            finally
            {
                // Ensure the connection is closed
                conn.Close();
            }

            return schedules;
        }
        public List<Schedule> GetSchedulesByCourseId(int courseId)
        {
            List<Schedule> schedules = new List<Schedule>();

            using (MySqlConnection conn = new MySqlConnection(_connectionString))
            {
                try
                {
                    conn.Open();

                    string sql = @"
                SELECT
                    s.id_schedule,
                    s.schedule,
                    s.fk_id_course,
                    c.id_course,
                    c.course_name,
                    c.course_detail,
                    c.course_price,
                    c.course_image,
                    c.fk_id_category,
                    cat.id_category,
                    cat.category_name,
                    cat.category_detail,
                    cat.category_image,
                    cat.category_banner,
                    cat.is_active
                FROM
                    tb_schedule AS s
                JOIN
                    tb_course AS c ON s.fk_id_course = c.id_course
                JOIN
                    tb_course_category AS cat ON c.fk_id_category = cat.id_category
                WHERE
                    s.fk_id_course = @FkIdCourse";

                    MySqlCommand cmd = new MySqlCommand(sql, conn);
                    cmd.Parameters.AddWithValue("@FkIdCourse", courseId);

                    using (MySqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Schedule schedule = new Schedule
                            {
                                IdSchedule = reader.GetInt32("id_schedule"),
                                ScheduleDate = DateTime.Parse(reader.GetString("schedule")),
                                FkIdCourse = reader.GetInt32("fk_id_course"),
                                Course = new Course // Assuming you have a Course class
                                {
                                    CourseId = reader.GetInt32("id_course"),
                                    CourseName = reader.GetString("course_name"),
                                    CourseDetail = reader.GetString("course_detail"),
                                    CoursePrice = reader.GetInt32("course_price"), // Assuming price is decimal
                                    CourseImage = reader.GetString("course_image"),
                                    Fk_Id_Category = reader.GetInt32("fk_id_category"),
                                    Category = new Category
                                    {
                                        IdCategory = reader.GetInt32("id_category"),
                                        CategoryName = reader.GetString("category_name"),
                                        CategoryDetail = reader.GetString("category_detail"),
                                        CategoryImage = reader.GetString("category_image"),
                                        CategoryBanner = reader.GetString("category_banner"),
                                        IsActive = reader.GetBoolean("is_active"),
                                    },
                                    // ... other properties
                                },
                            };
                            schedules.Add(schedule);
                        }
                    }
                }
                catch (MySqlException sqlEx)
                {
                    // Log SQL-specific exceptions
                    Console.WriteLine("SQL Error: " + sqlEx.ToString());
                    // Optionally, re-throw or handle differently
                    throw;
                }
                catch (Exception ex)
                {
                    // Log general exceptions
                    Console.WriteLine("An error occurred: " + ex.ToString());
                    // Optionally, re-throw the exception
                    // throw;
                }
                finally
                {
                    // Ensure the connection is closed
                    conn.Close();
                }
            }

            return schedules;
        }

        public Schedule Create(Schedule item)
        {
            MySqlConnection conn = new MySqlConnection(_connectionString);
            try
            {
                conn.Open();

                string sql = "INSERT INTO tb_schedule (schedule, fk_id_course) VALUES (@ScheduleDate, @FkIdCourse)";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@ScheduleDate", item.ScheduleDate);
                cmd.Parameters.AddWithValue("@FkIdCourse", item.FkIdCourse);
                cmd.ExecuteNonQuery();

                item.IdSchedule = (int)cmd.LastInsertedId;
            }
            catch (MySqlException sqlEx)
            {
                // Log SQL-specific exceptions
                Console.WriteLine("SQL Error: " + sqlEx.ToString());
                // Optionally, re-throw or handle differently
                throw;
            }
            catch (Exception ex)
            {
                // Log general exceptions
                Console.WriteLine("An error occurred: " + ex.ToString());
                // Optionally, re-throw the exception
                // throw;
            }
            finally
            {
                // Ensure the connection is closed
                conn.Close();
            }
            return item;
        }
    }
}
