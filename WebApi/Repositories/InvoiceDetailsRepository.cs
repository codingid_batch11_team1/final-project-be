﻿using MySql.Data.MySqlClient;
using Org.BouncyCastle.Bcpg.OpenPgp;
using WebApi.Models;

namespace WebApi.Repositories
{
    public class InvoiceDetailsRepository
    {
        private readonly string _connectionString = string.Empty;
        public InvoiceDetailsRepository(IConfiguration configuration)
        {
            _connectionString = configuration.GetConnectionString("Default");
        }
        public List<InvoiceDetails> GetAll(int? idInvoice, int idUser)
        {
            List<InvoiceDetails> invoiceDetails = new List<InvoiceDetails>();

            MySqlConnection conn = new MySqlConnection(_connectionString);
            try
            {
                conn.Open();

                string sql = "";

                if (idInvoice == 0)
                {
                    sql =
                        "SELECT " +
                        "inv_d.id_invoice_details, " +
                        "inv_d.fk_id_invoice, " +
                        "inv_d.fk_id_schedule, " +
                        "s.id_schedule, " +
                        "s.schedule, " +
                        "c.id_course, " +
                        "c.course_name, " +
                        "c.course_price, " +
                        "c.course_image, " +
                        "ct.id_category, " +
                        "ct.category_name, " +
                        "inv.id_invoice, " +
                        "inv.payment_date, " +
                        "u.id_user " +
                        "FROM " +
                        "tb_invoice_details AS inv_d " +
                        "JOIN " +
                        "tb_schedule AS s ON inv_d.fk_id_schedule = s.id_schedule " +
                        "JOIN " +
                        "tb_course AS c ON s.fk_id_course = c.id_course " +
                        "JOIN " +
                        "tb_course_category AS ct ON c.fk_id_category = ct.id_category " +
                        "JOIN " +
                        "tb_invoice AS inv ON inv_d.fk_id_invoice = inv.id_invoice " +
                        "JOIN " +
                        "tb_user As u ON inv.fk_id_user = u.id_user " +
                        "WHERE " +
                        "inv.fk_id_user = @IdUSer";

                }
                else if (idInvoice != null)
                {
                    sql =
                        "SELECT " +
                        "inv_d.id_invoice_details, " +
                        "inv_d.fk_id_invoice, " +
                        "inv_d.fk_id_schedule, " +
                        "s.id_schedule, " +
                        "s.schedule, " +
                        "c.id_course, " +
                        "c.course_name, " +
                        "c.course_price, " +
                        "c.course_image, " +
                        "ct.id_category, " +
                        "ct.category_name, " +
                        "inv.id_invoice, " +
                        "inv.payment_date, " +
                        "u.id_user " +
                        "FROM " +
                        "tb_invoice_details AS inv_d " +
                        "JOIN " +
                        "tb_schedule AS s ON inv_d.fk_id_schedule = s.id_schedule " +
                        "JOIN " +
                        "tb_course AS c ON s.fk_id_course = c.id_course " +
                        "JOIN " +
                        "tb_course_category AS ct ON c.fk_id_category = ct.id_category " +
                        "JOIN " +
                        "tb_invoice AS inv ON inv_d.fk_id_invoice = inv.id_invoice " +
                        "JOIN " +
                        "tb_user As u ON inv.fk_id_user = u.id_user " +
                        "WHERE " +
                        "inv_d.fk_id_invoice = @IdInvoice";
                }

                MySqlCommand cmd = new MySqlCommand(sql, conn);

                cmd.Parameters.AddWithValue("@IdInvoice", idInvoice);
                cmd.Parameters.AddWithValue("@IdUSer", idUser);

                MySqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    invoiceDetails.Add(new InvoiceDetails()
                    {
                        IdInvoiceDetails = reader.GetInt32("id_invoice_details"),
                        FkIdSchedule = reader.GetInt32("fk_id_schedule"),
                        FkIdInvoice = reader.GetInt32("fk_id_invoice"),

                        Schedule = new Schedule()
                        {
                            ScheduleDate = DateTime.Parse(reader.GetString("schedule")),
                        },//Schedule class

                        Course = new Course()
                        {
                            CourseId = reader.GetInt32("id_course"),
                            CourseName = reader.GetString("course_name"),
                            CoursePrice = reader.GetInt32("course_price"),
                            CourseImage = reader.GetString("course_image"),

                        },

                        Category = new Category()
                        {
                            IdCategory = reader.GetInt32("id_category"),
                            CategoryName = reader.GetString("category_name"),
                        },

                        Invoice = new Invoice()
                        {
                            IdInvoice = reader.GetInt32("id_invoice"),
                            PaymentDate = reader.GetDateTime("payment_date"),
                        },

                        User = new User()
                        {
                            IdUser = reader.GetInt32("id_user")
                        }
                    });
                }
            }
            catch (MySqlException sqlEx)
            {
                // Log SQL-specific exceptions
                Console.WriteLine("SQL Error: " + sqlEx.ToString());
                // Optionally, re-throw or handle differently
                throw;
            }
            catch (Exception ex)
            {
                // Log general exceptions
                Console.WriteLine("An error occurred: " + ex.ToString());
                // Optionally, re-throw the exception
                // throw;
            }
            finally
            {
                // Ensure the connection is closed
                conn.Close();
            }

            return invoiceDetails;
        }

        public List<InvoiceDetails> GetAllInvoiceDetailAdmin(int? idInvoice)
        {
            List<InvoiceDetails> invoiceDetails = new List<InvoiceDetails>();

            MySqlConnection conn = new MySqlConnection(_connectionString);
            try
            {
                conn.Open();

                string sql = "";

                if (idInvoice == 0)
                {
                    sql =
                         "SELECT " +
                         "inv_d.id_invoice_details, " +
                         "inv_d.fk_id_invoice, " +
                         "inv_d.fk_id_schedule, " +
                         "s.id_schedule, " +
                         "s.schedule, " +
                         "c.id_course, " +
                         "c.course_name, " +
                         "c.course_price, " +
                         "c.course_image, " +
                         "ct.id_category, " +
                         "ct.category_name, " +
                         "inv.id_invoice, " +
                         "inv.payment_date, " +
                         "u.id_user " +
                         "FROM " +
                         "tb_invoice_details AS inv_d " +
                         "JOIN " +
                         "tb_schedule AS s ON inv_d.fk_id_schedule = s.id_schedule " +
                         "JOIN " +
                         "tb_course AS c ON s.fk_id_course = c.id_course " +
                         "JOIN " +
                         "tb_course_category AS ct ON c.fk_id_category = ct.id_category " +
                         "JOIN " +
                         "tb_invoice AS inv ON inv_d.fk_id_invoice = inv.id_invoice " +
                         "JOIN " +
                         "tb_user As u ON inv.fk_id_user = u.id_user ";

                }
                else
                {
                    sql =
                        "SELECT " +
                        "inv_d.id_invoice_details, " +
                        "inv_d.fk_id_invoice, " +
                        "inv_d.fk_id_schedule, " +
                        "s.id_schedule, " +
                        "s.schedule, " +
                        "c.id_course, " +
                        "c.course_name, " +
                        "c.course_price, " +
                        "c.course_image, " +
                        "ct.id_category, " +
                        "ct.category_name, " +
                        "inv.id_invoice, " +
                        "inv.payment_date, " +
                        "u.id_user " +
                        "FROM " +
                        "tb_invoice_details AS inv_d " +
                        "JOIN " +
                        "tb_schedule AS s ON inv_d.fk_id_schedule = s.id_schedule " +
                        "JOIN " +
                        "tb_course AS c ON s.fk_id_course = c.id_course " +
                        "JOIN " +
                        "tb_course_category AS ct ON c.fk_id_category = ct.id_category " +
                        "JOIN " +
                        "tb_invoice AS inv ON inv_d.fk_id_invoice = inv.id_invoice " +
                        "JOIN " +
                        "tb_user As u ON inv.fk_id_user = u.id_user " +
                        "WHERE " +
                        "inv_d.fk_id_invoice = @IdInvoice";
                }

                MySqlCommand cmd = new MySqlCommand(sql, conn);

                cmd.Parameters.AddWithValue("IdInvoice", idInvoice);

                MySqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    invoiceDetails.Add(new InvoiceDetails()
                    {
                        IdInvoiceDetails = reader.GetInt32("id_invoice_details"),
                        FkIdSchedule = reader.GetInt32("fk_id_schedule"),
                        FkIdInvoice = reader.GetInt32("fk_id_invoice"),

                        Schedule = new Schedule()
                        {
                            ScheduleDate = DateTime.Parse(reader.GetString("schedule")),
                        },//Schedule class

                        Course = new Course()
                        {
                            CourseId = reader.GetInt32("id_course"),
                            CourseName = reader.GetString("course_name"),
                            CoursePrice = reader.GetInt32("course_price"),
                            CourseImage = reader.GetString("course_image"),

                        },

                        Category = new Category()
                        {
                            IdCategory = reader.GetInt32("id_category"),
                            CategoryName = reader.GetString("category_name"),
                        },

                        Invoice = new Invoice()
                        {
                            IdInvoice = reader.GetInt32("id_invoice"),
                            PaymentDate = reader.GetDateTime("payment_date"),
                        },

                        User = new User()
                        {
                            IdUser = reader.GetInt32("id_user")
                        }
                    });
                }
            }
            catch (MySqlException sqlEx)
            {
                // Log SQL-specific exceptions
                Console.WriteLine("SQL Error: " + sqlEx.ToString());
                // Optionally, re-throw or handle differently
                throw;
            }
            catch (Exception ex)
            {
                // Log general exceptions
                Console.WriteLine("An error occurred: " + ex.ToString());
                // Optionally, re-throw the exception
                // throw;
            }
            finally
            {
                // Ensure the connection is closed
                conn.Close();
            }

            return invoiceDetails;
        }
        public InvoiceDetails Create(InvoiceDetails item)
        {
            MySqlConnection conn = new MySqlConnection(_connectionString);
            try
            {
                conn.Open();

                string sql = "INSERT INTO tb_invoice_details (fk_id_invoice, fk_id_schedule) VALUES (@FkIdInvoice, @FkIdSchedule)";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@FkIdInvoice", item.FkIdInvoice);
                cmd.Parameters.AddWithValue("@FkIdSchedule", item.FkIdSchedule);
                cmd.ExecuteNonQuery();

                item.IdInvoiceDetails = (int)cmd.LastInsertedId;
            }
            catch (MySqlException sqlEx)
            {
                // Log SQL-specific exceptions
                Console.WriteLine("SQL Error: " + sqlEx.ToString());
                // Optionally, re-throw or handle differently
                throw;
            }
            catch (Exception ex)
            {
                // Log general exceptions
                Console.WriteLine("An error occurred: " + ex.ToString());
                // Optionally, re-throw the exception
                // throw;
            }
            finally
            {
                // Ensure the connection is closed
                conn.Close();
            }
            return item;
        }

        public InvoiceDetails? CheckIdSchedule(int? idUser, List<int>? idSchedules)
        {
            if (idUser == null || idSchedules == null || !idSchedules.Any())
            {
                return null;
            }

            InvoiceDetails? invoiceDetails = null;

            using (MySqlConnection conn = new MySqlConnection(_connectionString))
            {
                try
                {
                    conn.Open();
                    invoiceDetails = ExecuteInvoiceQuery(conn, idUser.Value, idSchedules);
                }
                catch (MySqlException sqlEx)
                {
                    // Log SQL-specific exceptions
                    Console.WriteLine("SQL Error: " + sqlEx.ToString());
                    // Optionally, re-throw or handle differently
                    throw;
                }
                catch (Exception ex)
                {
                    // Log general exceptions
                    Console.WriteLine("An error occurred: " + ex.ToString());
                    // Optionally, re-throw the exception
                    // throw;
                }
                finally
                {
                    // Ensure the connection is closed
                    conn.Close();
                }
            }

            return invoiceDetails;
        }

        private InvoiceDetails? ExecuteInvoiceQuery(MySqlConnection conn, int idUser, List<int> idSchedules)
        {
            InvoiceDetails? invoiceDetails = null;

            var parameters = new List<string>();
            for (int i = 0; i < idSchedules.Count; i++)
            {
                parameters.Add($"@IdSchedule{i}");
            }
            string inClause = string.Join(", ", parameters);

            string sql = $@"
                   SELECT
                       inv_d.fk_id_schedule,
                       i.fk_id_user
                   FROM
                       tb_invoice_details AS inv_d
                   JOIN
                       tb_invoice AS i ON inv_d.fk_id_invoice = i.id_invoice
                   WHERE
                       inv_d.fk_id_schedule IN ({inClause}) AND i.fk_id_user = @IdUser";

            using (MySqlCommand cmd = new MySqlCommand(sql, conn))
            {
                for (int i = 0; i < idSchedules.Count; i++)
                {
                    cmd.Parameters.AddWithValue($"@IdSchedule{i}", idSchedules[i]);
                }
                cmd.Parameters.AddWithValue("@IdUser", idUser);

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        invoiceDetails = new InvoiceDetails
                        {
                            FkIdSchedule = reader.GetInt32("fk_id_schedule"),
                            Invoice = new Invoice
                            {
                                FkIdUser = reader.GetInt32("fk_id_user"),
                            },
                        };
                    }
                }
            }

            return invoiceDetails;
        }

    }
}
