﻿using MySql.Data.MySqlClient;
using WebApi.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;

namespace WebApi.Repositories
{
    public class PaymentMethodRepository
    {
        private string connStr = string.Empty;
        public PaymentMethodRepository(IConfiguration configuration)
        {
            connStr = configuration.GetConnectionString("Default");
        }
        //"Default": "server=localhost;user=root;database=dlanguage;port=3306;password="

        public List<PaymentMethod> GetAllMethods()
        {
            List<PaymentMethod> payment = new List<PaymentMethod>();

            //connect to database
            MySqlConnection conn = new MySqlConnection(connStr);
            try
            {
                Console.WriteLine("Connecting to MySQL...");
                conn.Open();
                // Perform database operations
                MySqlCommand cmd = new MySqlCommand("SELECT * FROM tb_payment_method WHERE is_active=1", conn);
                MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    payment.Add(new PaymentMethod()
                    {
                        MethodId = reader.GetInt32("id_payment_method"),
                        MethodName = reader.GetString("method_name"),
                        MethodImage = reader.GetString("method_image"),
                        IsActive = reader.GetBoolean("is_active"),
                    });
                }
            }
            catch (MySqlException sqlEx)
            {
                // Log SQL-specific exceptions
                Console.WriteLine("SQL Error: " + sqlEx.ToString());
                // Optionally, re-throw or handle differently
                throw;
            }
            catch (Exception ex)
            {
                // Log general exceptions
                Console.WriteLine("An error occurred: " + ex.ToString());
                // Optionally, re-throw the exception
                // throw;
            }
            finally
            {
                // Ensure the connection is closed
                conn.Close();
            }
            Console.WriteLine("Done.");
            return payment;
        }

        public List<PaymentMethod> GetAllMethodsAdmin()
        {
            List<PaymentMethod> payment = new List<PaymentMethod>();

            //connect to database
            MySqlConnection conn = new MySqlConnection(connStr);
            try
            {
                Console.WriteLine("Connecting to MySQL...");
                conn.Open();
                // Perform database operations
                MySqlCommand cmd = new MySqlCommand("SELECT * FROM tb_payment_method", conn);
                MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    payment.Add(new PaymentMethod()
                    {
                        MethodId = reader.GetInt32("id_payment_method"),
                        MethodName = reader.GetString("method_name"),
                        MethodImage = reader.GetString("method_image"),
                        IsActive = reader.GetBoolean("is_active"),
                    });
                }
            }
            catch (MySqlException sqlEx)
            {
                // Log SQL-specific exceptions
                Console.WriteLine("SQL Error: " + sqlEx.ToString());
                // Optionally, re-throw or handle differently
                throw;
            }
            catch (Exception ex)
            {
                // Log general exceptions
                Console.WriteLine("An error occurred: " + ex.ToString());
                // Optionally, re-throw the exception
                // throw;
            }
            finally
            {
                // Ensure the connection is closed
                conn.Close();
            }
            Console.WriteLine("Done.");
            return payment;
        }

        public PaymentMethod Create(PaymentMethod data)
        {
            //get connection to database
            MySqlConnection conn = new MySqlConnection(connStr);
            try
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("INSERT INTO tb_payment_method (method_name, method_image, is_active) VALUES (@MethodName, @MethodImage, @IsActive)", conn);

                cmd.Parameters.AddWithValue("@MethodName", data.MethodName);
                cmd.Parameters.AddWithValue("@MethodImage", data.MethodImage);
                cmd.Parameters.AddWithValue("@IsActive", data.IsActive);

                cmd.ExecuteNonQuery();

                data.MethodId = (int)cmd.LastInsertedId;
            }
            catch (MySqlException sqlEx)
            {
                // Log SQL-specific exceptions
                Console.WriteLine("SQL Error: " + sqlEx.ToString());
                // Optionally, re-throw or handle differently
                throw;
            }
            catch (Exception ex)
            {
                // Log general exceptions
                Console.WriteLine("An error occurred: " + ex.ToString());
                // Optionally, re-throw the exception
                // throw;
            }
            finally
            {
                // Ensure the connection is closed
                conn.Close();
            }
            return data;
            
        }

        public PaymentMethod Update(PaymentMethod data)
        {
            MySqlConnection conn = new MySqlConnection(connStr);
            try
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("UPDATE tb_payment_method SET method_name=@MethodName, method_image=@MethodImage, is_active=@IsActive WHERE id_payment_method=@MethodId", conn);

                cmd.Parameters.AddWithValue("@MethodName", data.MethodName);
                cmd.Parameters.AddWithValue("@MethodImage", data.MethodImage);
                cmd.Parameters.AddWithValue("@IsActive", data.IsActive);
                cmd.Parameters.AddWithValue("@MethodId", data.MethodId); // Assuming MethodId is the primary key

                int rowsAffected = cmd.ExecuteNonQuery();

                if (rowsAffected < 1)
                {
                    // Handle the case where no rows were updated (e.g., record not found)
                    throw new Exception("Failed to update. Record not found.");
                }
            }
            catch (MySqlException sqlEx)
            {
                // Log SQL-specific exceptions
                Console.WriteLine("SQL Error: " + sqlEx.ToString());
                // Optionally, re-throw or handle differently
                throw;
            }
            catch (Exception ex)
            {
                // Log general exceptions
                Console.WriteLine("An error occurred: " + ex.ToString());
                // Optionally, re-throw the exception
                // throw;
            }
            finally
            {
                // Ensure the connection is closed
                conn.Close();
            }

            return data;
        }

    }
}
