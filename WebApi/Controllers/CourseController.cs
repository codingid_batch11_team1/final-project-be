﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using WebApi.Dtos;
using WebApi.Models;
using WebApi.Repositories;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CourseController : ControllerBase
    {

        private readonly CourseRepository _courseRepository;
        private readonly IWebHostEnvironment _webHostEnvironment;
        
        public CourseController(CourseRepository courseRepository, IWebHostEnvironment webHostEnvironment)
        {
            _courseRepository = courseRepository;
            _webHostEnvironment = webHostEnvironment;
        }

        [HttpGet]
        public IActionResult GetAllCourse(int? limit, int? categoryId, int? courseId)
        {
            try
            {
                var courses = _courseRepository.GetAll(limit, categoryId, courseId);
                return Ok(courses);
            }
            catch (Exception ex)
            {
                // Optionally, log the exception details for internal analysis
                return StatusCode(500, "Internal Server Error");
            }
        }

        [HttpGet("admin")]
        public IActionResult GetAllCourseAdmin()
        {
            try
            {
                var courses = _courseRepository.GetAllAdmin();
                return Ok(courses);
            }
            catch (Exception ex)
            {
                // Optionally, log the exception details for internal analysis
                return StatusCode(500, "Internal Server Error");
            }
        }

        [HttpGet("{id}")]
        public IActionResult GetCourseById(int id)
        {
            try
            {
                Course? course = _courseRepository.GetCourseById(id);

                if (course == null)
                {
                    return NotFound();
                }

                return Ok(course);
            }
            catch (Exception ex)
            {
                // Optionally, log the exception details for internal analysis
                return StatusCode(500, "Internal Server Error");
            }
        }

        [Authorize(Roles = "ADMIN")]
        [HttpPost]
        public async Task<IActionResult> CreateCourse([FromForm] AddCourseDto data)
        {
            try
            {
                IFormFile image = data.CourseImage!;

                var ext = Path.GetExtension(image.FileName).ToLowerInvariant();
                string fileName = Guid.NewGuid().ToString() + ext;

                string uploadDir = "uploads";
                string physicalPath = $"wwwroot/{uploadDir}";
                var filePath = Path.Combine(_webHostEnvironment.ContentRootPath, physicalPath, fileName);
                using var stream = System.IO.File.Create(filePath);
                await image.CopyToAsync(stream);

                string fileUrlPath = $"{uploadDir}/{fileName}";

                var courses = _courseRepository.Create(new Course
                {
                    CourseName = data.CourseName,
                    CourseDetail = data.CourseDetail,
                    Fk_Id_Category = data.Fk_Id_Category,
                    CoursePrice = data.CoursePrice,
                    CourseImage = fileUrlPath,
                    IsActive = data.IsActive,
                });

                return Ok(courses);
            }
            catch (Exception ex)
            {
                // Optionally, log the exception details for internal analysis
                return StatusCode(500, "Internal Server Error");
            }
        }

        [Authorize(Roles = "ADMIN")]
        [HttpPatch]
        public async Task<IActionResult> UpdateCourse(int id, [FromForm] UpdateCourseDto data)
        {
            if (data.CourseImage != null && data.CourseImage.Length > 0)
            {
                try
                {
                    IFormFile image = data.CourseImage!;

                    var ext = Path.GetExtension(image.FileName).ToLowerInvariant();
                    string fileName = Guid.NewGuid().ToString() + ext;

                    string uploadDir = "uploads";
                    string physicalPath = Path.Combine(_webHostEnvironment.ContentRootPath, "wwwroot", uploadDir);
                    var filePath = Path.Combine(physicalPath, fileName);

                    // Ensure the directory exists
                    Directory.CreateDirectory(physicalPath);

                    using (var stream = new FileStream(filePath, FileMode.Create))
                    {
                        await image.CopyToAsync(stream);
                    }

                    string fileUrlPath = $"{uploadDir}/{fileName}";

                    var courses = _courseRepository.Update(new Course
                    {
                        CourseId = id,
                        CourseName = data.CourseName,
                        CourseDetail = data.CourseDetail,
                        Fk_Id_Category = data.Fk_Id_Category,
                        CoursePrice = data.CoursePrice,
                        CourseImage = fileUrlPath,
                        IsActive = data.IsActive,
                    });

                    return Ok(courses);
                }
                catch (Exception ex)
                {
                    // Handle exceptions, log errors, etc.
                    return StatusCode(500, $"Internal server error: {ex.Message}");
                }
            }

            // Handle the case where data.CourseImage is null or empty
            // You might want to skip the file upload in this case or return an appropriate response.
            return BadRequest("CourseImage is required");
        }

        [Authorize(Roles = "ADMIN")]
        [HttpDelete("{id}")]
        public IActionResult DeleteCourseType(int id)
        {
            try
            {
                var result = _courseRepository.Delete(id);
                return Ok(result);
            }
            catch (Exception ex)
            {
                // Optionally, log the exception details for internal analysis
                return StatusCode(500, "Internal Server Error");
            }
        }

    }
}
