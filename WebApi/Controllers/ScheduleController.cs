﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApi.Repositories;
using WebApi.Models;
using WebApi.Dtos;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ScheduleController : ControllerBase
    {
        private readonly ScheduleRepository _scheduleRepository;
        public ScheduleController(ScheduleRepository scheduleRepository)
        {
            _scheduleRepository = scheduleRepository;
        }

        [HttpGet]
        public IActionResult GetSchedule()
        {
            try
            {
                var schedule = _scheduleRepository.GetAll();
                return Ok(schedule);
            }
            catch (Exception ex)
            {
                // Optionally, log the exception details for internal analysis
                return StatusCode(500, "Internal Server Error");
            }
        }


        [HttpGet("{id}")]
        public IActionResult GetSchedulesByCourseId(int id)
        {
            try
            {
                var schedules = _scheduleRepository.GetSchedulesByCourseId(id);

                if (schedules == null || schedules.Count == 0) // If no schedules are found
                {
                    return NotFound();
                }

                return Ok(schedules);
            }
            catch (Exception ex)
            {
                // Optionally, log the exception details for internal analysis
                return StatusCode(500, "Internal Server Error");
            }
        }


        [HttpPost]
        public IActionResult CreateInvoiceDetails([FromBody] AddScheduleDto data)
        {
            try
            {
                var scheduleRepository = _scheduleRepository.Create(new Schedule
                {
                    ScheduleDate = data.ScheduleDate,
                    FkIdCourse = data.FkIdCourse,
                });

                return Ok(scheduleRepository);
            }
            catch (Exception ex)
            {
                // Optionally, log the exception details for internal analysis
                return StatusCode(500, "Internal Server Error");
            }
        }

    }
}
