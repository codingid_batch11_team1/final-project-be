﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using WebApi.Dtos;
using WebApi.Models;
using WebApi.Repositories;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class InvoiceDetailsController : ControllerBase
    {
        private readonly InvoiceDetailsRepository _invoiceDetailsRepository;
        public InvoiceDetailsController(InvoiceDetailsRepository invoiceDetailsRepository)
        {
            _invoiceDetailsRepository = invoiceDetailsRepository;
        }

        [Authorize]
        [HttpGet("{idInvoice}")]
        public IActionResult GetInvoiceDetails(int? idInvoice)
        {
            try
            {
                var userIdClaim = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Sid);
                if (userIdClaim == null)
                {
                    return Unauthorized("User ID claim not found."); // Or handle this appropriately
                }

                // Attempt to parse the user ID to an integer
                if (!int.TryParse(userIdClaim.Value, out int userId))
                {
                    return BadRequest("Invalid user ID."); // Or handle this appropriately
                }

                var invoiceDetails = _invoiceDetailsRepository.GetAll(idInvoice, userId);
                return Ok(invoiceDetails);
            }
            catch (Exception ex)
            {
                // Optionally, log the exception details for internal analysis
                return StatusCode(500, "Internal Server Error");
            }
        }

        [HttpGet("InvoiceDetailAdmin/{idInvoice}")]
        public IActionResult GetInvoiceDetailsAdmin(int? idInvoice)
        {
            try
            {
                var invoiceDetails = _invoiceDetailsRepository.GetAllInvoiceDetailAdmin(idInvoice);
                return Ok(invoiceDetails);
            }
            catch (Exception ex)
            {
                // Optionally, log the exception details for internal analysis
                return StatusCode(500, "Internal Server Error");
            }
        }

        [HttpPost]
        public IActionResult CreateInvoiceDetails([FromBody] AddInvoiceDetailsDto addInvoiceDetails)
        {
            try
            {
                var invoiceDetails = _invoiceDetailsRepository.Create(new InvoiceDetails
                {
                    FkIdInvoice = addInvoiceDetails.FkIdInvoice,
                    FkIdSchedule = addInvoiceDetails.FkIdSchedule,
                });

                return Ok(invoiceDetails);
            }
            catch (Exception ex)
            {
                // Optionally, log the exception details for internal analysis
                return StatusCode(500, "Internal Server Error");
            }
        }

    }
}
