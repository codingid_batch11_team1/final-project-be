﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using System.Security.Cryptography;
using WebApi.Dtos;
using WebApi.Models;
using WebApi.Repositories;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CheckoutController : ControllerBase
    {
        private readonly CheckoutRepository _checkoutRepository;
        private readonly InvoiceDetailsRepository _invoiceDetailsRepository;
        public CheckoutController(CheckoutRepository checkoutRepository, InvoiceDetailsRepository invoiceDetailsRepository)
        {
            _checkoutRepository = checkoutRepository;
            _invoiceDetailsRepository = invoiceDetailsRepository;
        }

        [Authorize]
        [HttpGet]
        public IActionResult GetCheckout()
        {
            try
            {
                var userIdClaim = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Sid);
                if (userIdClaim == null)
                {
                    return Unauthorized();
                }

                if (!int.TryParse(userIdClaim.Value, out int userId))
                {
                    return BadRequest();
                }

                var checkout = _checkoutRepository.GetCheckoutByIdUser(userId);

                return Ok(checkout);
            }
            catch
            {
                return StatusCode(500);
            }
        }

        [Authorize]
        [HttpPost]
        public IActionResult CreateCheckout([FromBody] AddCheckoutDto data)
        {
            try
            {
                var userIdClaim = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Sid);
                if (userIdClaim == null)
                {
                    return BadRequest();
                }

                if (!int.TryParse(userIdClaim.Value, out int userId))
                {
                    return BadRequest();
                }

                if (data.FkIdSchedule == 0)
                {
                    return BadRequest();
                }

                List<int> scheduleIds = new List<int> { data.FkIdSchedule };
                InvoiceDetails? invoiceDetails = _invoiceDetailsRepository.CheckIdSchedule(userId, scheduleIds);

                if (invoiceDetails != null)
                {
                    return BadRequest();
                }

                bool checkoutExists = _checkoutRepository.CheckoutExists(userId, data.FkIdSchedule);
                if (checkoutExists)
                {
                    return BadRequest();
                }

                var checkout = _checkoutRepository.Create(new Checkout
                {
                    FkIdUser = userId,
                    FkIdSchedule = data.FkIdSchedule,
                });

                if (checkout == null)
                {
                    return BadRequest();
                }

                return Ok(checkout);
            }
            catch
            {
                return StatusCode(500);
            }
        }


        [Authorize]
        [HttpPost("Pay")]
        public IActionResult PayCheckout([FromBody] PayCheckoutDto data)
        {
            if (data == null)
            {
                return BadRequest("Request data is null.");
            }

            var userIdClaim = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Sid);
            if (userIdClaim == null)
            {
                return Unauthorized("User ID claim not found.");
            }

            if (!int.TryParse(userIdClaim.Value, out int userId))
            {
                return BadRequest("Invalid user ID.");
            }

            if (data.idSchedules == null || data.idSchedules.Count == 0)
            {
                return BadRequest("No course selected.");
            }

            try
            {
                InvoiceDetails? invoiceDetails = _invoiceDetailsRepository.CheckIdSchedule(userId, data.idSchedules);

                if (invoiceDetails != null)
                {
                    return BadRequest("You already purchased the course.");
                }

                _checkoutRepository.CreateInvoiceAndInvoiceDetails(userId, data.idSchedules, data.IdPaymentMethod, data.idCheckouts);
                return Ok(data);
            }
            catch (Exception ex)
            {
                // Log the exception details
                // Consider returning a generic error message to the client
                return StatusCode(500, "An error occurred while processing your request.");
            }
        }


        [Authorize]
        [HttpPatch("{id}")]
        public IActionResult UpdateCheckout(int id, [FromBody] AddCheckoutDto addCheckoutDto)
        {
            try
            {
                var checkout = _checkoutRepository.Update(new Checkout
                {
                    IdCheckout = id,
                    FkIdUser = addCheckoutDto.FkIdUser,
                    FkIdSchedule = addCheckoutDto.FkIdSchedule,
                });

                return Ok(checkout);
            }
            catch
            {
                return StatusCode(500, new { error = "An error occurred while processing the request." });
            }
        }

        [Authorize]
        [HttpDelete]
        public IActionResult DeleteCheckout([FromBody] DeleteCheckoutDto data)
        {
            try
            {
                return Ok(_checkoutRepository.Delete(data.IdCheckout));
            }
            catch
            {
                return StatusCode(500, new { error = "An error occurred while processing the request." });
            }
        }

    }
}
