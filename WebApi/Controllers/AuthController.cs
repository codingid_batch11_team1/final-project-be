﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApi.Dto;
using WebApi.Models;
using WebApi.Repositories;
using WebApi.Helpers;
using WebApi.Dtos;
using Microsoft.AspNetCore.Authorization;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly UserRepository _userRepository;

        public AuthController(UserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        [HttpPost("Login")]
        public IActionResult Login([FromBody] LoginDto data)
        {
            try
            {
                string hashedPassword = PasswordHelper.EncryptPassword(data.Password);

                User? user = _userRepository.GetByEmailAndPassword(data.Email, hashedPassword);

                if (user == null)
                {
                    return NotFound();
                }

                // create token
                string token = JWTHelper.Generate(user.IdUser, user.Role);

                return Ok(token);
            }
            catch (Exception ex)
            {
                // Log the exception or handle it as needed
                return StatusCode(500, new { error = "An error occurred while processing the request." });
            }
        }

        [HttpPost("Register")]
        public async Task<IActionResult> Register([FromBody] RegisterDto data)
        {
            try
            {
                string hashedPassword = PasswordHelper.EncryptPassword(data.Password);

                User? user = _userRepository.GetByEmail(data.Email);

                if (user != null)
                {
                    return BadRequest("Email already registered.");
                }

                _userRepository.Create(data.Username, data.Email, hashedPassword);

            string emailConfirmationLink = "http://localhost:5173/emailConfirmSuccess/?email=" + data.Email;

                await MailHelper.Send(data.Username, data.Email, "Email Confirmation", "Hello " + data.Email + ", Your Email Confirmation link: " + emailConfirmationLink);

                return Ok();
            }
            catch (Exception ex)
            {
                // Log the exception or handle it as needed
                return StatusCode(500, new { error = "An error occurred while processing the request." });
            }
        }


        [HttpPatch("isActive")]
        public IActionResult IsActive([FromBody] UpdateIsActiveDto data)
        {
            try
            {
                User? user = _userRepository.GetByEmail(data.Email);

                if (user == null)
                {
                    return BadRequest("User not found.");
                }

                bool isSuccess = _userRepository.UpdateIsActive(user.IdUser, true);

                if (!isSuccess)
                {
                    return Problem();
                }

                return Ok("User isActive status updated successfully.");
            }
            catch (Exception ex)
            {
                // Log the exception or handle it as needed
                return StatusCode(500, new { error = "An error occurred while processing the request." });
            }
        }


        [HttpPost("ForgotPassword")]
        public async Task<IActionResult> ForgotPassword([FromBody] ForgotPasswordDto data)
        {
            try
            {
                // Check if email is valid
                User? user = _userRepository.GetByEmail(data.Email);
                if (user == null)
                {
                    return NotFound();
                }

                string key = DateTime.UtcNow.Ticks.ToString() + data.Email;
                string resetToken = PasswordHelper.EncryptPassword(key);

                // Update reset token
                bool isSuccess = _userRepository.InsertResetPasswordToken(user.IdUser, resetToken);

                if (!isSuccess)
                {
                    return Problem();
                }

            string resetLink = "http://localhost:5173/newpass/?email=" + data.Email + "&token=" + resetToken;

                await MailHelper.Send(user.Username, data.Email, "Forgot Password", "Hello " + user.Email + ", Your reset password link: " + resetLink);

                return Ok();
            }
            catch (Exception ex)
            {
                // Log the exception or handle it as needed
                return StatusCode(500, new { error = "An error occurred while processing the request." });
            }
        }


        [HttpPost("ResetPassword")]
        public async Task<IActionResult> ResetPassword([FromBody] ResetPasswordDto data)
        {
            try
            {
                // Check if email is valid
                User? user = _userRepository.GetByEmailAndResetToken(data.Email, data.Token);
                if (user == null)
                {
                    return NotFound();
                }

                string hashedPassword = PasswordHelper.EncryptPassword(data.NewPassword);

                bool isResetSuccess = _userRepository.UpdatePassword(user.IdUser, hashedPassword);

                if (!isResetSuccess)
                {
                    return BadRequest("Failed to reset the password");
                }

                await MailHelper.Send(user.Username, data.Email, "Reset Password Success", "Hello " + user.Email + ", Your new password is: " + data.NewPassword);

                return Ok();
            }
            catch (Exception ex)
            {
                // Log the exception or handle it as needed
                return StatusCode(500, new { error = "An error occurred while processing the request." });
            }
        }


        [HttpGet("AllUser")]
        public IActionResult GetAllUsers()
        {
            try
            {
                var users = _userRepository.GetAllUsers();
                return Ok(users);
            }
            catch (Exception ex)
            {
                // Log the exception or handle it as needed
                return StatusCode(500, new { error = "An error occurred while processing the request." });
            }
        }

        [Authorize(Roles = "ADMIN")]
        [HttpPost("CreateUserByAdmin")]
        public IActionResult CreateUserByAdmin([FromBody] AddUserByAdminDto data)
        {
            try
            {
                var user = _userRepository.CreateUserByAdmin(new User
                {
                    Username = data.Username,
                    Email = data.Email,
                    Role = data.Role,
                    Password = data.Password,
                    is_Active = data.IsActive,
                });
                return Ok(user);
            }
            catch (Exception ex)
            {
                // Log the exception or handle it as needed
                return StatusCode(500, new { error = "An error occurred while processing the request." });
            }
        }

        [Authorize(Roles = "ADMIN")]
        [HttpPatch("UpdateUserById")]
        public IActionResult UpdateUserStatusById(int id, [FromBody] UpdateUserStatusDto data)
        {
            try
            {
                var user = _userRepository.UpdateUserStatus(new User
                {
                    IdUser = id,
                    Username = data.Username,
                    Email = data.Email,
                    Role = data.Role,
                    is_Active = data.is_Active
                });
                return Ok(user);
            }
            catch (Exception ex)
            {
                // Log the exception or handle it as needed
                return StatusCode(500, new { error = "An error occurred while processing the request." });
            }
        }

    }
}
