﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApi.Dtos;
using WebApi.Models;
using WebApi.Repositories;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class InvoiceController : ControllerBase
    {
        private readonly InvoiceRepository _invoiceRepository;
        public InvoiceController(InvoiceRepository invoiceRepository)
        {
            _invoiceRepository = invoiceRepository;
        }

        [Authorize]
        [HttpGet("{idUser}")]
        public IActionResult GetAllInvoices(int idUser)
        {
            try
            {
                var invoices = _invoiceRepository.GetAll(idUser);
                return Ok(invoices);
            }
            catch (Exception ex)
            {
                // Optionally, log the exception details for internal analysis
                return StatusCode(500, "Internal Server Error");
            }
        }

        [HttpGet("InvoiceAdmin")]
        public IActionResult GetInvoiceAdmin()
        {
            try
            {
                var invoiceAdmin = _invoiceRepository.GetAllInvoiceAdmin();
                return Ok(invoiceAdmin);
            }
            catch (Exception ex)
            {
                // Optionally, log the exception details for internal analysis
                return StatusCode(500, "Internal Server Error");
            }
        }

        [HttpPost]
        public IActionResult CreateCourse([FromBody] AddInvoiceDto addInvoiceDto)
        {
            try
            {
                var invoices = _invoiceRepository.Create(new Invoice
                {
                    PaymentDate = addInvoiceDto.PaymentDate,
                    FkIdUser = addInvoiceDto.FkIdUser,
                    FkidPaymentMethod = addInvoiceDto.FkidPaymentMethod
                });

                return Ok(invoices);
            }
            catch (Exception ex)
            {
                // Optionally, log the exception details for internal analysis
                return StatusCode(500, "Internal Server Error");
            }
        }

    }
}
