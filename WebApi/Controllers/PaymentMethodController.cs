﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.IO;
using System.Threading.Tasks;
using WebApi.Dtos;
using WebApi.Models;
using WebApi.Repositories;
using Microsoft.AspNetCore.Authorization;

//By Rey, please tell me if there is an error
namespace WebApi.Controllers
{
    [ApiController]
    [Route("api/payment-methods")]
    public class PaymentMethodController : ControllerBase
    {
        private readonly IWebHostEnvironment _webHostEnvironment;
        private readonly PaymentMethodRepository _paymentMethodRepository;

        public PaymentMethodController(IWebHostEnvironment webHostEnvironment, PaymentMethodRepository paymentMethodRepository)
        {
            _webHostEnvironment = webHostEnvironment;
            _paymentMethodRepository = paymentMethodRepository;
        }

        [HttpGet]
        public IActionResult GetAllMethods()
        {
            try
            {
                var methods = _paymentMethodRepository.GetAllMethods();
                return Ok(methods);
            }
            catch (Exception ex)
            {
                // Optionally, log the exception details for internal analysis
                return StatusCode(500, "Internal Server Error");
            }
        }

        [HttpGet("AllMethodsByAdmin")]
        public IActionResult GetAllMethodsAdmin()
        {
            try
            {
                var methods = _paymentMethodRepository.GetAllMethodsAdmin();
                return Ok(methods);
            }
            catch (Exception ex)
            {
                // Optionally, log the exception details for internal analysis
                return StatusCode(500, "Internal Server Error");
            }
        }

        [Authorize(Roles = "ADMIN")]
        [HttpPost]
        public async Task<IActionResult> CreatePayment([FromForm] PaymentMethodDto data)
        {
            try
            {
                if (data.MethodImage == null || data.MethodImage.Length == 0)
                {
                    return BadRequest("No file uploaded.");
                }

                IFormFile image = data.MethodImage;

                // Generate a unique file name
                var ext = Path.GetExtension(image.FileName).ToLowerInvariant();
                string fileName = Guid.NewGuid().ToString() + ext;

                // Specify the upload directory and physical path
                string uploadDir = "PaymentMethod";
                string physicalPath = Path.Combine(_webHostEnvironment.ContentRootPath, "wwwroot", uploadDir);
                var filePath = Path.Combine(physicalPath, fileName);

                // Create the directory if it doesn't exist
                if (!Directory.Exists(physicalPath))
                {
                    Directory.CreateDirectory(physicalPath);
                }

                // Save the file to the server
                using (var stream = System.IO.File.Create(filePath))
                {
                    await image.CopyToAsync(stream);
                }

                // Construct the file URL path
                string fileUrlPath = $"/{uploadDir}/{fileName}";

                // Create a PaymentMethod object
                var paymentMethod = new PaymentMethod
                {
                    MethodName = data.MethodName,
                    MethodImage = fileUrlPath,
                    IsActive = data.IsActive
                };

                // Save the PaymentMethod to the repository
                _paymentMethodRepository.Create(paymentMethod);

                return Ok();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error saving image: " + ex.ToString());
                return StatusCode(500, "Error saving image.");
            }
        }

        [HttpPatch]
        public async Task<IActionResult> UpdatePayment(int id, [FromForm] PaymentMethodDto data)
        {
            try
            {
                if (data.MethodImage == null || data.MethodImage.Length == 0)
                {
                    return BadRequest("No file uploaded.");
                }

                IFormFile image = data.MethodImage;

                // Generate a unique file name
                var ext = Path.GetExtension(image.FileName).ToLowerInvariant();
                string fileName = Guid.NewGuid().ToString() + ext;

                // Specify the upload directory and physical path
                string uploadDir = "PaymentMethod";
                string physicalPath = Path.Combine(_webHostEnvironment.ContentRootPath, "wwwroot", uploadDir);
                var filePath = Path.Combine(physicalPath, fileName);

                // Create the directory if it doesn't exist
                if (!Directory.Exists(physicalPath))
                {
                    Directory.CreateDirectory(physicalPath);
                }

                // Save the file to the server
                using (var stream = System.IO.File.Create(filePath))
                {
                    await image.CopyToAsync(stream);
                }

                // Construct the file URL path
                string fileUrlPath = $"/{uploadDir}/{fileName}";

                // Create a PaymentMethod object
                var paymentMethod = new PaymentMethod
                {
                    MethodId = id,
                    MethodName = data.MethodName,
                    MethodImage = fileUrlPath,
                    IsActive = data.IsActive
                };

                // Save the PaymentMethod to the repository
                _paymentMethodRepository.Update(paymentMethod);

                return Ok();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error saving image: " + ex.ToString());
                return StatusCode(500, "Error saving image.");
            }
        }
    }
}
