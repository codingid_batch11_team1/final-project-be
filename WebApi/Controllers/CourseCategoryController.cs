﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Configuration;
using WebApi.Dtos;
using WebApi.Models;
using WebApi.Repositories;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CourseCategoryController : ControllerBase
    {
        private readonly IWebHostEnvironment _webHostEnvironment;
        private readonly CategoryRepository _categoryRepository;
        public CourseCategoryController(CategoryRepository categoryRepository, IWebHostEnvironment webHostEnvironment)
        {
            _categoryRepository = categoryRepository;
            _webHostEnvironment = webHostEnvironment;
        }

        [HttpGet]
        public IActionResult GetCategory()
        {
            try
            {
                var categories = _categoryRepository.GetAll();
                return Ok(categories);
            }
            catch
            {
                return StatusCode(500);
            }
        }

        [HttpGet("admin")]
        public IActionResult GetCategoryAdmin()
        {
            try
            {
                var categories = _categoryRepository.GetAllAdmin();
                return Ok(categories);
            }
            catch
            {
                return StatusCode(500);
            }
        }

        [HttpGet("{id}")]
        public IActionResult GetCategoryById(int id)
        {
            try
            {
                Category? category = _categoryRepository.GetCategoryById(id);

                if (category == null)
                {
                    return NotFound();
                }

                return Ok(category);
            }
            catch
            {
                return StatusCode(500);
            }
        }


        [Authorize(Roles = "ADMIN")]
        [HttpPost]
        public async Task<IActionResult> CreateCategory([FromForm] AddCategoryDto data)
        {
            try
            {
                /*================== upload image ==================*/
                var image = data.CategoryImage;
                var banner = data.CategoryBanner;

                var ext = Path.GetExtension(image.FileName).ToLowerInvariant(); //.jpg

                // get filename
                string fileName = Guid.NewGuid().ToString() + ext;
                string fileName2 = Guid.NewGuid().ToString() + ext; // pasti unik
                string uploadDir = "Category"; // foldering biar 
                string physicalPath = Path.Combine(_webHostEnvironment.ContentRootPath, "wwwroot", uploadDir);

                // saving image
                var imagePath = Path.Combine(physicalPath, fileName);
                using (var stream = System.IO.File.Create(imagePath))
                {
                    await image.CopyToAsync(stream);
                }

                // saving banner
                var bannerPath = Path.Combine(physicalPath, fileName2);
                using (var stream = System.IO.File.Create(bannerPath))
                {
                    await banner.CopyToAsync(stream);
                }

                // create url path
                string fileUrlPath = $"{uploadDir}/{fileName}";
                string fileBannerPath = $"{uploadDir}/{fileName2}";
                /*================== upload image ==================*/

                var category = _categoryRepository.Create(new Category
                {
                    CategoryName = data.CategoryName,
                    CategoryDetail = data.CategoryDetail,
                    CategoryImage = fileUrlPath,
                    CategoryBanner = fileBannerPath,
                    IsActive = data.IsActive,
                });

                return Ok(category);
            }
            catch (Exception ex)
            {
                // Log or handle the exception appropriately
                return StatusCode(500, "Internal server error");
            }
        }

        [Authorize(Roles = "ADMIN")]
        [HttpPatch]
        public async Task<IActionResult> UpdateCategory(int id, [FromForm] UpdateCategoryDto data)
        {
            try
            {
                var image = data.CategoryImage;
                var banner = data.CategoryBanner;

                var ext = Path.GetExtension(image.FileName).ToLowerInvariant();

                string fileName = Guid.NewGuid().ToString() + ext;
                string fileName2 = Guid.NewGuid().ToString() + ext;
                string uploadDir = "Category";
                string physicalPath = Path.Combine(_webHostEnvironment.ContentRootPath, "wwwroot", uploadDir);

                var imagePath = Path.Combine(physicalPath, fileName);
                using (var stream = System.IO.File.Create(imagePath))
                {
                    await image.CopyToAsync(stream);
                }

                var bannerPath = Path.Combine(physicalPath, fileName2);
                using (var stream = System.IO.File.Create(bannerPath))
                {
                    await banner.CopyToAsync(stream);
                }

                string fileUrlPath = $"{uploadDir}/{fileName}";
                string fileBannerPath = $"{uploadDir}/{fileName2}";

                var category = _categoryRepository.Update(new Category
                {
                    IdCategory = id,
                    CategoryName = data.CategoryName,
                    CategoryDetail = data.CategoryDetail,
                    CategoryImage = fileUrlPath,
                    CategoryBanner = fileBannerPath,
                    IsActive = data.IsActive,
                });

                return Ok(category);
            }
            catch
            {
                return StatusCode(500);
            }
        }

        [Authorize(Roles = "ADMIN")]
        [HttpDelete("{id}")]
        public IActionResult DeleteCourseType(int id)
        {
            try
            {
                return Ok(_categoryRepository.Delete(id));
            }
            catch
            {
                return StatusCode(500);
            }
        }

    }
}