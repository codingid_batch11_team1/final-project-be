﻿using MimeKit;
using MailKit.Net.Smtp;
using MailKit.Security;
using System.Threading.Tasks;

namespace WebApi.Helpers
{
    public class MailHelper
    {
        public static async Task Send(string Username, string toEmail, string subject, string messageText)
        {
            var message = new MimeMessage();
            message.From.Add(new MailboxAddress("No Reply", "noreply@otomobil.id"));
            message.To.Add(new MailboxAddress(Username, toEmail));
            message.Subject = subject;

            // Create the message body
            var body = new TextPart("plain")
            {
                Text = messageText
            };
            message.Body = body;

            // Initialize and configure the SMTP client
            using (var smtp = new SmtpClient())
            {
                await smtp.ConnectAsync("smtp.gmail.com", 587, SecureSocketOptions.StartTls); // Replace with your SMTP server and port
                await smtp.AuthenticateAsync("noreply.otomobil@gmail.com", " klpndfvbhqpbfswu"); // Replace with your SMTP username and password

                // Send the email
                await smtp.SendAsync(message);

                // Disconnect from the SMTP server
                await smtp.DisconnectAsync(true);
            }
        }
    }
}
