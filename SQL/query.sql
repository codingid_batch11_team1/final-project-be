
INSERT INTO tb_course_type (type_name, type_detail, type_image) VALUES
	("Electric", "Loren-loren", "/images/electic.png"),
	("Hatchback", "Mera-Mera Nomi", "/images/hatchback.png"),
	("LCGC", "Mera-Mera Nomi", "/images/lcgc.png"),
	("MPV", "Mera-Mera Nomi", "/images/mpv.png"),
	("Offroad", "Mera-Mera Nomi", "/images/offroad.png"),
	("Sedan", "Mera-Mera Nomi", "/images/sedan.png"),
	("Truck", "Truck-kun", "/images/truck.png");
	
SET  @type_id := 0;

UPDATE tb_course_type SET type_id = @type_id := (@type_id+1);

ALTER TABLE tb_course_type AUTO_INCREMENT =1; 


INSERT INTO tb_course (course_name, course_detail, course_price, fk_id_category, course_image) 
VALUES
	("Course Mitsubishi Pajero", "mobil orang kayah, penguasa jalanan", 800000, 1, "/images/pajero.png"),
	("Dump Truck for Mining Constructor", "big boys", 1200000, 8, "/images/dump_truck.png"),
	("Sedan Honda Civic", "cool boys", 400000, 7, "/images/honda_civic.png");
	
CREATE TABLE tb_user (
	id_user INT PRIMARY KEY AUTO_INCREMENT,
	username VARCHAR(255),
	email VARCHAR(255),
	PASSWORD VARCHAR(255),
	is_active BOOLEAN
);

CREATE TABLE tb_schedule (
	id_schedule INT PRIMARY KEY AUTO_INCREMENT,
	`date` DATE,
	fk_course_id INT
);

CREATE TABLE tb_checkout (
	id_checkout INT PRIMARY KEY AUTO_INCREMENT,
	fk_id_user INT,
	fk_id_schedule INT,
	fk_course_id INT
); 
