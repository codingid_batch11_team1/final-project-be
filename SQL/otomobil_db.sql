-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 29, 2023 at 09:52 AM
-- Server version: 10.4.28-MariaDB
-- PHP Version: 8.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `otomobil_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_checkout`
--

CREATE TABLE `tb_checkout` (
  `id_checkout` int(11) NOT NULL,
  `fk_id_user` int(11) NOT NULL,
  `fk_id_schedule` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_course`
--

CREATE TABLE `tb_course` (
  `id_course` int(11) NOT NULL,
  `course_name` varchar(255) DEFAULT NULL,
  `course_detail` text DEFAULT NULL,
  `course_price` int(11) DEFAULT NULL,
  `course_image` varchar(255) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT 1,
  `fk_id_category` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `tb_course`
--

INSERT INTO `tb_course` (`id_course`, `course_name`, `course_detail`, `course_price`, `course_image`, `is_active`, `fk_id_category`) VALUES
(1, 'Course SUV Kijang Innova', 'Mobil kencang andalan keluarga', 700000, '/images/kijang.jpg', 1, 1),
(2, 'Hyundai Palisade 2021', 'Mobil irit andalan bapak-bapak', 800000, '/images/Hyundai_Palisade_2021.png', 1, 1),
(3, 'Course LCGC Honda Brio', 'kuning', 500000, '/images/honda_brio.png', 1, 4),
(4, 'Course Mitsubishi Pajero', 'mobil orang kaya, penguasa jalanan', 800000, '/images/pajero.png', 1, 1),
(5, 'Dump Truck for Mining Constructor', 'big boys', 1200000, '/images/dump_truck.png', 1, 8),
(6, 'Sedan Honda Civic', 'cool boys', 400000, '/images/honda_civic.png', 1, 7);

-- --------------------------------------------------------

--
-- Table structure for table `tb_course_category`
--

CREATE TABLE `tb_course_category` (
  `id_category` int(11) NOT NULL,
  `category_name` varchar(255) NOT NULL,
  `category_detail` text NOT NULL,
  `category_image` varchar(255) NOT NULL,
  `category_banner` varchar(255) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `tb_course_category`
--

INSERT INTO `tb_course_category` (`id_category`, `category_name`, `category_detail`, `category_image`, `category_banner`, `is_active`) VALUES
(1, 'SUV', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '/images/try.png', '', 1),
(2, 'Electric', 'Loren-loren', '/images/electric.png', '', 1),
(3, 'Hatchback', 'Mera-Mera Nomi', '/images/hatchback.png', '', 1),
(4, 'LCGC', 'Mera-Mera Nomi', '/images/lcgc.png', '', 1),
(5, 'MPV', 'Mera-Mera Nomi', '/images/mpv.png', '', 1),
(6, 'Offroad', 'Mera-Mera Nomi', '/images/offroad.png', '', 1),
(7, 'Sedan', 'Mera-Mera Nomi', '/images/sedan.png', '', 1),
(8, 'Truck', 'Truck-kun', '/images/truck.png', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_invoice`
--

CREATE TABLE `tb_invoice` (
  `id_invoice` int(11) NOT NULL,
  `payment_date` datetime DEFAULT NULL,
  `fk_id_user` int(11) DEFAULT NULL,
  `fk_id_payment_method` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_invoice_details`
--

CREATE TABLE `tb_invoice_details` (
  `id_invoice_details` int(11) NOT NULL,
  `fk_id_invoice` int(11) NOT NULL,
  `fk_id_schedule` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_payment_method`
--

CREATE TABLE `tb_payment_method` (
  `id_payment_method` int(11) NOT NULL,
  `method_name` varchar(255) DEFAULT NULL,
  `method_image` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_schedule`
--

CREATE TABLE `tb_schedule` (
  `id_schedule` int(11) NOT NULL,
  `schedule` datetime NOT NULL,
  `fk_id_course` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `tb_schedule`
--

INSERT INTO `tb_schedule` (`id_schedule`, `schedule`, `fk_id_course`) VALUES
(1, '2023-10-29 09:49:22', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE `tb_user` (
  `id_user` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` varchar(25) NOT NULL DEFAULT 'USER',
  `is_active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_checkout`
--
ALTER TABLE `tb_checkout`
  ADD PRIMARY KEY (`id_checkout`),
  ADD KEY `tb_checkout_ibfk_1` (`fk_id_user`),
  ADD KEY `tb_checkout_ibfk_2` (`fk_id_schedule`);

--
-- Indexes for table `tb_course`
--
ALTER TABLE `tb_course`
  ADD PRIMARY KEY (`id_course`),
  ADD KEY `fk_id_category` (`fk_id_category`);

--
-- Indexes for table `tb_course_category`
--
ALTER TABLE `tb_course_category`
  ADD PRIMARY KEY (`id_category`);

--
-- Indexes for table `tb_invoice`
--
ALTER TABLE `tb_invoice`
  ADD PRIMARY KEY (`id_invoice`),
  ADD KEY `fk_id_user` (`fk_id_user`),
  ADD KEY `fk_id_payment_method` (`fk_id_payment_method`);

--
-- Indexes for table `tb_invoice_details`
--
ALTER TABLE `tb_invoice_details`
  ADD PRIMARY KEY (`id_invoice_details`),
  ADD KEY `tb_invoice_details_ibfk_1` (`fk_id_invoice`),
  ADD KEY `tb_invoice_details_ibfk_2` (`fk_id_schedule`);

--
-- Indexes for table `tb_payment_method`
--
ALTER TABLE `tb_payment_method`
  ADD PRIMARY KEY (`id_payment_method`);

--
-- Indexes for table `tb_schedule`
--
ALTER TABLE `tb_schedule`
  ADD PRIMARY KEY (`id_schedule`),
  ADD KEY `tb_schedule_ibfk_1` (`fk_id_course`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id_user`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_checkout`
--
ALTER TABLE `tb_checkout`
  MODIFY `id_checkout` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_course`
--
ALTER TABLE `tb_course`
  MODIFY `id_course` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tb_course_category`
--
ALTER TABLE `tb_course_category`
  MODIFY `id_category` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tb_invoice`
--
ALTER TABLE `tb_invoice`
  MODIFY `id_invoice` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_invoice_details`
--
ALTER TABLE `tb_invoice_details`
  MODIFY `id_invoice_details` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_payment_method`
--
ALTER TABLE `tb_payment_method`
  MODIFY `id_payment_method` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_schedule`
--
ALTER TABLE `tb_schedule`
  MODIFY `id_schedule` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tb_checkout`
--
ALTER TABLE `tb_checkout`
  ADD CONSTRAINT `tb_checkout_ibfk_1` FOREIGN KEY (`fk_id_user`) REFERENCES `tb_user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_checkout_ibfk_2` FOREIGN KEY (`fk_id_schedule`) REFERENCES `tb_schedule` (`id_schedule`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_course`
--
ALTER TABLE `tb_course`
  ADD CONSTRAINT `tb_course_ibfk_1` FOREIGN KEY (`fk_id_category`) REFERENCES `tb_course_category` (`id_category`);

--
-- Constraints for table `tb_invoice`
--
ALTER TABLE `tb_invoice`
  ADD CONSTRAINT `tb_invoice_ibfk_1` FOREIGN KEY (`fk_id_user`) REFERENCES `tb_user` (`id_user`),
  ADD CONSTRAINT `tb_invoice_ibfk_2` FOREIGN KEY (`fk_id_payment_method`) REFERENCES `tb_payment_method` (`id_payment_method`);

--
-- Constraints for table `tb_invoice_details`
--
ALTER TABLE `tb_invoice_details`
  ADD CONSTRAINT `tb_invoice_details_ibfk_1` FOREIGN KEY (`fk_id_invoice`) REFERENCES `tb_invoice` (`id_invoice`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_invoice_details_ibfk_2` FOREIGN KEY (`fk_id_schedule`) REFERENCES `tb_schedule` (`id_schedule`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tb_schedule`
--
ALTER TABLE `tb_schedule`
  ADD CONSTRAINT `tb_schedule_ibfk_1` FOREIGN KEY (`fk_id_course`) REFERENCES `tb_course` (`id_course`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
